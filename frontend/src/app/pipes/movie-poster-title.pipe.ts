import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'moviePosterTitle'
})
export class MoviePosterTitlePipe implements PipeTransform {

  transform(value: string, ...args: any[]): unknown {
    if (value.length > 50) {
      return value.substring(0, 50) + '...';
    }
    return value;
  }

}
