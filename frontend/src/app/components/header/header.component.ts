import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  moviesListLink = 'movies/';
  searchQuery: string;
  searchBarIsShown = false;
  username: string;

  constructor(private router: Router, private userService: UserService) {
    if (userService.isUserAuthenticated(userService.getAuthenticationToken())) {
      this.username = userService.getAuthenticatedUsername();
    }
  }

  ngOnInit(): void {
  }

  setSearchBarVisibility(visible: boolean) {
    this.searchBarIsShown = visible;
  }

  onSearchClick() {
    this.searchBarIsShown = false;
    this.router.navigate(['search'], {
      queryParams: {query: this.searchQuery}
    });
    this.searchQuery = '';
  }

  logout(event: Event) {
    event.preventDefault();
    this.userService.logoutUser();
    this.router.navigate(['home']).then(() => window.location.reload());
  }
}
