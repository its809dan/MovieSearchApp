import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  isUserAuthenticated: boolean;

  constructor(private userService: UserService) {
    this.isUserAuthenticated = userService.isUserAuthenticated(userService.getAuthenticationToken());
  }

  ngOnInit(): void {
  }

}
