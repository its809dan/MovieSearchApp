import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserSettings} from '../../models/user-settings';
import { mustMatch } from 'src/app/helpers/must-match.validator';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.css']
})
export class UserSettingsComponent implements OnInit {
  monthsOptions = [
    '-', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
  ];
  currentYear = new Date().getFullYear();
  fieldErrorStyle: '1px #ff0000 solid';

  settingsForm: FormGroup;
  currentEmail: string;

  constructor(private formBuilder: FormBuilder, private userService: UserService, private router: Router) {
    this.settingsForm = formBuilder.group({
      email: ['', Validators.email],
      birthDay: [1],
      birthMonth: ['-'],
      birthYear: [this.currentYear],
      password: ['', [Validators.minLength(8), Validators.maxLength(128)]],
      passwordConfirm: ['']
    }, {
      validators: mustMatch('password', 'passwordConfirm')
    });
  }

  ngOnInit(): void {
    this.userService.getUserInfo(this.userService.getAuthenticatedUsername()).subscribe(resp => {
      if (resp.status >= 200 && resp.status < 300) {
        this.settingsForm.get('email').setValue(resp.body.email);
        this.currentEmail = resp.body.email;
        if (resp.body.birthdate) {
          const birthDateParts = resp.body.birthdate.split('-');
          this.settingsForm.get('birthYear').setValue(birthDateParts[0]);
          this.settingsForm.get('birthMonth').setValue(this.monthsOptions[Number.parseInt(birthDateParts[1], 10)]);
          this.settingsForm.get('birthDay').setValue(birthDateParts[2]);
        }
      }
    });
  }

  getFormField(fieldName: string) {
    return this.settingsForm.get(fieldName);
  }

  isSettingsFieldValid(fieldName: string): boolean {
    return this.settingsForm.get(fieldName).valid;
  }

  updateUser() {
    if (this.settingsForm.valid) {
      const userSettings = new UserSettings();
      if (this.settingsForm.get('email').value && this.settingsForm.get('email').valid) {
        userSettings.email = this.settingsForm.get('email').value;
      }
      if (this.settingsForm.get('birthMonth').value !== this.monthsOptions[0]) {
        userSettings.birthDay = this.settingsForm.get('birthDay').value;
        userSettings.birthMonth = this.monthsOptions.indexOf(this.settingsForm.get('birthMonth').value);
        userSettings.birthYear = this.settingsForm.get('birthYear').value;
      }
      if (this.settingsForm.get('password').value && this.settingsForm.get('password').valid &&
        this.settingsForm.get('passwordConfirm').valid) {
        userSettings.password = this.settingsForm.get('password').value;
      }
      this.userService.updateUserInfo(userSettings).subscribe(resp => {
        if (resp.status >= 200 && resp.status < 300) {
          window.location.reload();
        }
      });
    }
  }

  deleteAccount() {
    const confirmation = confirm('Do you really want to delete the account?');
    if (confirmation) {
      this.userService.deleteAccount().subscribe(resp => {
        if (resp.status >= 200 && resp.status < 300) {
          this.userService.logoutUser();
          this.router.navigate(['home']).then(() => window.location.reload());
        }
      });
    }
  }

  checkIfEmailIsTaken() {
    if (this.settingsForm.get('email').value !== this.currentEmail) {
      this.userService.checkIfEmailExists(this.settingsForm.get('email').value).subscribe(resp => {
        if (resp.status >= 200 && resp.status < 300) {
          if (resp.body.taken) {
            this.settingsForm.get('email').setErrors({taken: true});
          }
        }
      });
    }
  }
}
