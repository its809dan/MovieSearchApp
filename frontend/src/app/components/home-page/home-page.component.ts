import { Component, OnInit } from '@angular/core';
import {MoviePoster} from '../../models/the-movie-db/movie-poster';
import {MovieService} from '../../services/movie.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  homeMoviesList: MoviePoster[];

  constructor(private moviesService: MovieService) { }

  ngOnInit(): void {
    this.moviesService.getHomePageMovies().subscribe(value => this.homeMoviesList = value.results.slice(0, 10));
  }

}
