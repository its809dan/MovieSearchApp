import {Component, Input, OnInit} from '@angular/core';
import {MoviePoster} from '../../models/the-movie-db/movie-poster';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.css']
})
export class MoviesListComponent implements OnInit {
  @Input()
  movies: MoviePoster[];

  constructor() { }

  ngOnInit(): void {
  }

}
