import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MoviePoster} from '../../models/the-movie-db/movie-poster';
import {UserCollectionMovie} from '../../models/user-collection-movie';
import {UserMoviesService} from '../../services/user-movies.service';

@Component({
  selector: 'app-user-movies-collection',
  templateUrl: './user-movies-collection.component.html',
  styleUrls: ['./user-movies-collection.component.css']
})
export class UserMoviesCollectionComponent implements OnInit {
  movies: MoviePoster[];
  type: string;

  constructor(private activatedRoute: ActivatedRoute, private userMoviesService: UserMoviesService) { }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      switch (data.collectionType) {
        case 'favorites':
          this.type = 'Favorites';
          this.userMoviesService.getUserFavorites().subscribe(this.fetchMovies);
          break;
        case 'watchLater':
          this.type = 'Watch List';
          this.userMoviesService.getUserWatchLater().subscribe(this.fetchMovies);
      }
    });
  }

  fetchMovies = (response) => {
    if (response.status >= 200 && response.status < 300) {
      this.movies = this.convertMoviePoster(response.body);
    }
  };

  convertMoviePoster(userMovies: UserCollectionMovie[]): MoviePoster[] {
    return userMovies.map(movie => {
      const poster = new MoviePoster();
      poster.id = movie.id;
      poster.poster_path = movie.posterUrl;
      poster.title = movie.title;
      return poster;
    });
  }

}
