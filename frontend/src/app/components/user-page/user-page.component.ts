import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';
import {User} from '../../models/user';
import {ActivatedRoute} from '@angular/router';
import {USER_NO_PROFILE_PHOTO_PATH} from '../../constants/consts';
import {CommentService} from '../../services/comment.service';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent implements OnInit {
  user: User;
  commentsCount: number;
  profilePicture = USER_NO_PROFILE_PHOTO_PATH;

  constructor(private activatedRoute: ActivatedRoute, private userService: UserService,
              private commentService: CommentService) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.userService.getUserInfo(params.username).subscribe(resp => {
        if (resp.status >= 200 && resp.status < 300) {
          this.user = resp.body;
        }
      });
      this.commentService.getUserCommentsCount(params.username).subscribe(resp => {
        if (resp.status >= 200 && resp.status < 300) {
          this.commentsCount = resp.body;
        }
      });
    });
  }

}
