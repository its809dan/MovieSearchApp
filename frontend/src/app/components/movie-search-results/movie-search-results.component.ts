import { Component, OnInit } from '@angular/core';
import {MovieService} from '../../services/movie.service';
import {MoviePoster} from '../../models/the-movie-db/movie-poster';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-movie-search-results',
  templateUrl: './movie-search-results.component.html',
  styleUrls: ['./movie-search-results.component.css']
})
export class MovieSearchResultsComponent implements OnInit {
  totalResultsCount: number;
  foundMovies: MoviePoster[];
  page = 1;
  query: string;

  constructor(private moviesService: MovieService,
              private activatedRoute: ActivatedRoute) {
    this.totalResultsCount = 0;
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(queryParams => {
      if (queryParams.query) {
        this.query = queryParams.query;
        this.moviesService.getMoviesBySearchQuery(queryParams.query, this.page).subscribe(value => {
          this.totalResultsCount = value.total_results;
          this.foundMovies = value.results;
        });
      }
    });
  }

  loadMoreFound() {
    this.page++;
    this.moviesService.getMoviesBySearchQuery(this.query, this.page).subscribe(value => {
      for (const result of value.results) {
        this.foundMovies.push(result);
      }
    });
  }
}
