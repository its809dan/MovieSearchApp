import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {
  searchQuery: string;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  onSearchClick() {
    if (!this.searchQuery) {
      alert('Input something in the search field');
    } else {
      this.router.navigate(['search'], {
        queryParams: {query: this.searchQuery}
      });
    }
  }
}
