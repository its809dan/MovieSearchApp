import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {mustMatch} from '../../helpers/must-match.validator';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  signUpForm: FormGroup;

  fieldNotEmptyMessage = 'This field is required';
  fieldErrorStyle = '1px #ff0000 solid';

  loginFormErrorMessage: string;

  constructor(private formBuilder: FormBuilder, private userService: UserService,
              private router: Router) {
    this.loginForm = formBuilder.group({
      loginUsername: ['', [Validators.required]],
      loginPassword: ['', Validators.required]
    });
    this.signUpForm = formBuilder.group({
      signupEmail: ['', [Validators.required, Validators.email]],
      signupUsername: ['', [Validators.required, Validators.maxLength(30), Validators.pattern('^[a-zA-Z0-9\\._]+$')]],
      signupPassword: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(128)]],
      signupConfirmPassword: ['', Validators.required]
    }, {
      validators: mustMatch('signupPassword', 'signupConfirmPassword')
    });
  }

  ngOnInit(): void {
  }

  isLoginFormFieldValid(fieldName: string) {
    return this.loginForm.controls[fieldName].untouched || this.loginForm.controls[fieldName].valid;
  }

  isSignupFormFieldValid(fieldName: string) {
    return this.signUpForm.controls[fieldName].untouched || this.signUpForm.controls[fieldName].valid;
  }

  getLoginFormField(fieldName: string) {
    return this.loginForm.get(fieldName);
  }

  getSignupFormField(fieldName: string) {
    return this.signUpForm.get(fieldName);
  }

  login() {
    if (this.loginForm.valid) {
      const username = this.loginForm.get('loginUsername').value;
      const password = this.loginForm.get('loginPassword').value;
      this.userService.loginUser({username, password}).subscribe(resp => {
        const status = resp.status;
        if (status >= 200 && status < 300) {
          localStorage.setItem('authData', JSON.stringify(resp.body));
          // Promise.resolve(this.location.back()).then(() => window.location.reload());
          this.router.navigate(['home']).then(() => window.location.reload());
        }
      }, error => {
        this.loginFormErrorMessage = 'Wrong username or password';
      });
    }
  }

  signUp() {
    const email = this.signUpForm.get('signupEmail').value;
    const username = this.signUpForm.get('signupUsername').value;
    const password = this.signUpForm.get('signupPassword').value;
    this.userService.signUpUser({email, username, password}).subscribe(resp => {
      const status = resp.status;
      if (status >= 200 && status < 300) {
        alert('Success! User signed up');
        window.location.reload();
      } else if (status >= 400 && status < 500) {
        alert(resp.error.message);
      }
    });
  }

  checkIfEmailExists(input) {
    const email = input.target.value;
    if (email) {
      this.userService.checkIfEmailExists(email).subscribe(resp => {
        if (resp.status >= 200 && resp.status < 300) {
          if (resp.body.taken) {
            this.signUpForm.get('signupEmail').setErrors({taken: true});
          }
        }
      });
    }
  }

  checkIfUsernameExists(input) {
    console.log(this.signUpForm.get('signupUsername').errors);
    const username = input.target.value;
    if (username) {
      this.userService.checkIfUsernameExists(username).subscribe(resp => {
        if (resp.status >= 200 && resp.status < 300) {
          if (resp.body.taken) {
            this.signUpForm.get('signupUsername').setErrors({taken: true});
          }
        }
      });
    }
  }
}
