import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Options} from 'ng5-slider';
import {FilterOptions} from '../../models/the-movie-db/filter-options';
import {MoviesSorting} from '../../models/the-movie-db/movies-sorting';
import {Genre} from '../../models/the-movie-db/genre';

@Component({
  selector: 'app-movie-search-filters',
  templateUrl: './movie-search-filters.component.html',
  styleUrls: ['./movie-search-filters.component.css']
})
export class MovieSearchFiltersComponent implements OnInit {
  @Input()
  sortingOptions: MoviesSorting[];

  @Input()
  genres: Genre[];

  @Input()
  minUserScore: number;

  @Input()
  maxUserScore: number;

  @Input()
  optionsUserScore: Options;

  @Input()
  minRuntime: number;

  @Input()
  maxRuntime: number;

  @Input()
  optionsRuntime: Options;

  @Output()
  useSearchFilters = new EventEmitter();

  selectedFilters: FilterOptions = new FilterOptions();

  constructor() { }

  ngOnInit(): void {
  }

  onSearchBtnClick(filterOptions: FilterOptions) {
    this.useSearchFilters.emit(filterOptions);
  }

  setMoviesSorting(sorting: MoviesSorting) {
    this.selectedFilters.sortType = sorting;
  }

  toggleGenreFilter(genre: Genre) {
    const foundGenre = this.selectedFilters.genres.filter(value => value.id === genre.id)[0];
    foundGenre ?
      this.selectedFilters.genres.splice(this.selectedFilters.genres.indexOf(foundGenre), 1) :
      this.selectedFilters.genres.push(genre);
  }

  setMinUserScore(value: number) {
    this.selectedFilters.minUserScore = value;
  }

  setMaxUserScore(value: number) {
    this.selectedFilters.maxUserScore = value;
  }

  setMinRuntime(value: number) {
    this.selectedFilters.minRuntime = value;
  }

  setMaxRuntime(value: number) {
    this.selectedFilters.maxRuntime = value;
  }

  setFromReleaseDate(event) {
    this.selectedFilters.fromReleaseDate = event.target.value;
  }

  setToReleaseDate(event) {
    this.selectedFilters.toReleaseDate = event.target.value;
  }

  getSelectedGenresIds(): number[] {
    return this.selectedFilters.genres.map(value => value.id);
  }
}
