import { Component, OnInit } from '@angular/core';
import {MovieService} from '../../services/movie.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MoviePoster} from '../../models/the-movie-db/movie-poster';
import {
  MOVIE_MAX_RUNTIME_IN_MIN,
  MOVIE_MAX_USER_SCORE,
  MOVIE_MIN_RUNTIME_IN_MIN,
  MOVIE_MIN_USER_SCORE,
  MOVIES_SEARCH_PAGE_SIZE
} from '../../constants/consts';
import {Genre} from '../../models/the-movie-db/genre';
import {Options} from 'ng5-slider';
import {FilterOptions} from '../../models/the-movie-db/filter-options';
import {MoviesSortingTypes} from '../../constants/movies-sorting-types';

@Component({
  selector: 'app-movies-collection',
  templateUrl: './movies-collection.component.html',
  styleUrls: ['./movies-collection.component.css']
})
export class MoviesCollectionComponent implements OnInit {
  isFiltersBarShown: boolean;
  sortingOptions = Object.values(MoviesSortingTypes);
  genres: Genre[];

  minUserScore = MOVIE_MIN_USER_SCORE;
  maxUserScore = MOVIE_MAX_USER_SCORE;
  optionsUserScore: Options = {
    floor: MOVIE_MIN_USER_SCORE,
    ceil: MOVIE_MAX_USER_SCORE,
    step: 0.1,
    noSwitching: true
  };

  minRuntime = MOVIE_MIN_RUNTIME_IN_MIN;
  maxRuntime = MOVIE_MAX_RUNTIME_IN_MIN;
  optionsRuntime: Options = {
    floor: MOVIE_MIN_RUNTIME_IN_MIN,
    ceil: MOVIE_MAX_RUNTIME_IN_MIN,
    step: 15,
    noSwitching: true
  };

  selectedFilters: FilterOptions;
  displayedMoviesCollection: MoviePoster[];
  currentPage: number;
  pageSize = MOVIES_SEARCH_PAGE_SIZE;
  totalResults: number;
  totalPages: number;

  constructor(private moviesService: MovieService,
              private activatedRoute: ActivatedRoute,
              private router: Router) {
    this.moviesService.getAllGenres().subscribe(value => this.genres = value.genres);
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.isFiltersBarShown = data.withSearch;
      this.currentPage = data.moviesCollection.page;
      this.displayedMoviesCollection = data.moviesCollection.results;
      this.totalResults = data.moviesCollection.total_results;
      this.totalPages = data.moviesCollection.total_pages;
    });
  }

  onPageChange(page: number) {
    this.currentPage = page;
    this.router.navigate([], {
      queryParams: {
        ...this.activatedRoute.snapshot.queryParams,
        page: this.currentPage
      },
    });
    window.scroll(0, 0);
  }

  onWithFiltersSearch(selectedFilters: FilterOptions) {
    this.selectedFilters = selectedFilters;
    this.currentPage = 1;
    this.moviesService.getMoviesBySelectedFilters(selectedFilters, this.currentPage).subscribe(value => {
      this.currentPage = value.page;
      this.displayedMoviesCollection = value.results;
      this.totalResults = value.total_results;
      this.totalPages = value.total_pages;
      this.router.navigate([], {
        queryParams: {page: this.currentPage}
      });
    }, () => {
      this.router.navigate(['not-found']);
    });
  }
}
