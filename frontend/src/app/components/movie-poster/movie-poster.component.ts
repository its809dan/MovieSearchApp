import {Component, Input, OnInit} from '@angular/core';
import {MoviePoster} from '../../models/the-movie-db/movie-poster';
import {MOVIE_DB_POSTER_SOURCE, MOVIE_NO_POSTER_PATH} from '../../constants/consts';

@Component({
  selector: 'app-movie-poster',
  templateUrl: './movie-poster.component.html',
  styleUrls: ['./movie-poster.component.css']
})
export class MoviePosterComponent implements OnInit {
  @Input()
  movie: MoviePoster;
  posterSource = MOVIE_DB_POSTER_SOURCE;
  noPosterPath = MOVIE_NO_POSTER_PATH;

  constructor() {

  }

  ngOnInit(): void {

  }

}
