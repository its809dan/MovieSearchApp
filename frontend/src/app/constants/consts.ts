export const MOVIE_DB_POSTER_SOURCE = 'https://image.tmdb.org/t/p/w600_and_h900_bestv2';

export const ACTOR_PROFILE_PHOTO = 'https://image.tmdb.org/t/p/w600_and_h900_bestv2';

export const MOVIE_BACKDROP_SOURCE = 'https://image.tmdb.org/t/p/w533_and_h300_bestv2';

export const ACTOR_NO_PROFILE_PHOTO_PATH = '../../assets/images/actor-no-profile.jpg';

export const USER_NO_PROFILE_PHOTO_PATH = '../../assets/images/user-no-profile.png';

export const MOVIE_NO_POSTER_PATH = '../../assets/images/no-poster.jpg';

export const MOVIES_SEARCH_PAGE_SIZE = 20;

export const MOVIE_MIN_USER_SCORE = 0;

export const MOVIE_MAX_USER_SCORE = 10;

export const MOVIE_MIN_RUNTIME_IN_MIN = 0;

export const MOVIE_MAX_RUNTIME_IN_MIN = 400;

export const SERVICE_URL_BASE = 'http://localhost:8080';
