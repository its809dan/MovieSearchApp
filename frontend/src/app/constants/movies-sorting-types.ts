import {MoviesSorting} from '../models/the-movie-db/movies-sorting';

export const MoviesSortingTypes = {
  POPULARITY_DESCENDING: new MoviesSorting('Popularity Descending', 'popularity.desc'),
  POPULARITY_ASCENDING: new MoviesSorting('Popularity Ascending', 'popularity.asc'),
  RATING_DESCENDING: new MoviesSorting('Rating Descending', 'vote_average.desc'),
  RATING_ASCENDING: new MoviesSorting('Rating Ascending', 'vote_average.asc'),
  RELEASE_DATE_DESCENDING: new MoviesSorting('Release Date Descending', 'release_date.desc'),
  RELEASE_DATE_ASCENDING: new MoviesSorting('Release Date Ascending', 'release_date.asc'),
  TITLE_A_Z: new MoviesSorting('Title (A-Z)', 'original_title.asc'),
  TITLE_Z_A: new MoviesSorting('Title (Z-A)', 'original_title.desc')
};
