import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MovieService} from '../../../../services/movie.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MovieInfo} from '../../../../models/the-movie-db/movie-info';
import {MOVIE_BACKDROP_SOURCE, MOVIE_DB_POSTER_SOURCE, MOVIE_NO_POSTER_PATH} from '../../../../constants/consts';
import {ActorCard} from '../../../../models/the-movie-db/actor-card';
import {MovieReview} from '../../../../models/the-movie-db/movie-review';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {MovieSelectedStatus} from '../../../../models/movie-selected-status';
import {UserService} from '../../../../services/user.service';
import {HttpResponse} from '@angular/common/http';
import {CommentService} from '../../../../services/comment.service';
import {Comment} from '../../../../models/comment';
import {CommentRequest} from '../../../../models/comment-request';
import {UserMoviesService} from '../../../../services/user-movies.service';

@Component({
  selector: 'app-movie-info',
  templateUrl: './movie-info.component.html',
  styleUrls: ['./movie-info.component.css']
})
export class MovieInfoComponent implements OnInit {
  movie: MovieInfo;
  cast: ActorCard[];
  bestReview: MovieReview;
  posterSource = MOVIE_DB_POSTER_SOURCE;
  noPosterPath = MOVIE_NO_POSTER_PATH;
  mainTrailerLink: string;
  allTrailersLinks: string[];
  backdropsSource = MOVIE_BACKDROP_SOURCE;
  allBackdropsLinks: string[];
  allPostersLinks: string[];

  selectedStatus: MovieSelectedStatus;
  username: string;
  @ViewChild('commentInput') commentInput: ElementRef;
  comments: Comment[];
  commentsCount: number;
  editingComment: Comment = null;

  constructor(private moviesService: MovieService, private userService: UserService,
              private userMoviesService: UserMoviesService, private commentService: CommentService,
              private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.username = this.userService.getAuthenticatedUsername();
    this.activatedRoute.params.subscribe(params => {
      this.activatedRoute.data.subscribe(data => {
        this.movie = data.movieInfo;
        if (data.movieSelectedStatus) {
          this.getSelectedStatus(data.movieSelectedStatus);
        }
        if (data.movieTrailers) {
          if (data.movieTrailers.results[0]) {
            this.allTrailersLinks = data.movieTrailers.results.map(object => object.key);
            this.mainTrailerLink = data.movieTrailers.results[0].key;
          }
        }
        this.getActors(params.id);
        this.getReview(params.id);
        this.getImages(params.id);
      }, () => {
        this.router.navigate(['not-found']);
      });
      this.commentService.getMovieComments(params.id).subscribe(resp => {
        if (resp.status >= 200 && resp.status < 300) {
          this.comments = resp.body.map(comment => ({...comment, user: comment.user.username}));
        }
      });
      this.commentService.getMovieCommentsCount(params.id).subscribe(resp => {
        if (resp.status >= 200 && resp.status < 300) {
          this.commentsCount = resp.body;
        }
      });
    });
  }

  getSelectedStatus(response: HttpResponse<any>) {
    if (response.status >= 200 && response.status < 300) {
      this.selectedStatus = response.body;
    }
  }

  getActors(id: number) {
    this.moviesService.getActorsByMovieId(id).subscribe(credits => {
      this.cast = credits.cast.slice(0, 10);
    });
  }

  getReview(id: number) {
    this.moviesService.getReviewsByMovieId(id).subscribe(reviews => {
      if (reviews.results[0]) {
        this.bestReview = reviews.results[0];
        this.bestReview.paragraphs = this.bestReview.content.split('\r\n\r\n');
      }
    });
  }

  getImages(id: number) {
    this.moviesService.getImagesByMovieId(id).subscribe(images => {
      if (images.backdrops.length) {
        this.allBackdropsLinks = images.backdrops.map(object => object.file_path);
      }
      if (images.posters.length) {
        this.allPostersLinks = images.posters.map(object => object.file_path);
      }
    });
  }

  toggleFavorites() {
    if (!this.selectedStatus.inFavorites) {
      this.userMoviesService.addMovieToFavorites(this.movie).subscribe(() => {
        this.selectedStatus.inFavorites = true;
      });
    } else {
      this.userMoviesService.deleteMovieFromFavorites(this.movie.id).subscribe(() => {
        this.selectedStatus.inFavorites = false;
      });
    }
  }

  toggleWatchlist() {
    if (!this.selectedStatus.inWatchLater) {
      this.userMoviesService.addMovieToWatchLater(this.movie).subscribe(() => {
        this.selectedStatus.inWatchLater = true;
      });
    } else {
      this.userMoviesService.deleteMovieFromWatchLater(this.movie.id).subscribe(() => {
        this.selectedStatus.inWatchLater = false;
      });
    }
  }

  openTrailerPopup(content) {
    this.modalService.open(content);
  }

  addComment() {
    const input = this.commentInput.nativeElement.value;
    if (input !== '') {
      const comment = new CommentRequest();
      comment.content = input;
      comment.movie = UserMoviesService.movieInfoTransform(this.movie);
      if (this.editingComment) {
        comment.id = this.editingComment.id;
        this.commentService.editComment(comment).subscribe(resp => {
          if (resp.status >= 200 && resp.status < 300) {
            const commentIndex = this.comments.findIndex(comm => comm.id === comment.id);
            this.comments[commentIndex].content = resp.body.content;
            this.comments[commentIndex].publishDateTime = resp.body.publishDateTime;
            this.editingComment = null;
          }
        });
      } else {
        this.commentService.addComment(comment).subscribe(resp => {
          if (resp.status >= 200 && resp.status < 300) {
            this.comments.push({...resp.body, user: resp.body.user.username});
            this.commentsCount++;
          }
        });
      }
      this.commentInput.nativeElement.value = '';
    }
  }

  deleteComment(comment: Comment) {
    this.commentService.deleteComment(comment.id).subscribe(resp => {
      if (resp.status >= 200 && resp.status < 300) {
        const commentIndex = this.comments.findIndex(comm => comm.id === comment.id);
        this.comments.splice(commentIndex, 1);
        this.commentsCount--;
      }
    });
  }

  editComment(comment: Comment) {
    this.editingComment = comment;
    this.commentInput.nativeElement.value = comment.content;
  }
}
