import {Component, Input, OnInit} from '@angular/core';
import {ActorCard} from '../../../../models/the-movie-db/actor-card';
import {ACTOR_PROFILE_PHOTO, ACTOR_NO_PROFILE_PHOTO_PATH} from '../../../../constants/consts';

@Component({
  selector: 'app-movie-actor-card',
  templateUrl: './movie-actor-card.component.html',
  styleUrls: ['./movie-actor-card.component.css']
})
export class MovieActorCardComponent implements OnInit {
  @Input()
  actorCard: ActorCard;
  profilePhoto = ACTOR_PROFILE_PHOTO;
  noProfilePhoto = ACTOR_NO_PROFILE_PHOTO_PATH;

  constructor() { }

  ngOnInit(): void {
  }

}
