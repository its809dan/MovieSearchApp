import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.css']
})
export class StarRatingComponent implements OnInit {
  @Input()
  starsCount: number;
  @Input()
  filledStarsCount: number;

  constructor() {
  }

  ngOnInit(): void {
    this.filledStarsCount = Math.round(this.filledStarsCount);
  }

  createRange(count: number): number[] {
    const items: number[] = [];
    for (let i = 0; i < Math.round(count); i++) {
      items.push(i + 1);
    }
    return items;
  }

}
