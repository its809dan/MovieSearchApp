import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Comment} from '../../../../models/comment';

@Component({
  selector: 'app-comments-section',
  templateUrl: './comments-section.component.html',
  styleUrls: ['./comments-section.component.css']
})
export class CommentsSectionComponent implements OnInit {
  @Input()
  comments: Comment[];

  @Input()
  username: string;

  @Output()
  editComment: EventEmitter<any> = new EventEmitter();

  @Output()
  deleteComment: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  editClick(comment: Comment) {
    this.editComment.emit(comment);
  }

  deleteClick(comment: Comment) {
    this.deleteComment.emit(comment);
  }
}
