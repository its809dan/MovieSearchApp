import {Component, Input, OnInit} from '@angular/core';
import {ActorCard} from '../../../../models/the-movie-db/actor-card';

@Component({
  selector: 'app-actors-carousel',
  templateUrl: './actors-carousel.component.html',
  styleUrls: ['./actors-carousel.component.css']
})
export class ActorsCarouselComponent implements OnInit {
  @Input()
  actorsCards: ActorCard[];

  constructor() { }

  ngOnInit(): void {
  }

}
