import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ActorsCarouselComponent} from './components/actors-carousel/actors-carousel.component';
import {CommentsSectionComponent} from './components/comments-section/comments-section.component';
import {MovieActorCardComponent} from './components/movie-actor-card/movie-actor-card.component';
import {MovieInfoComponent} from './components/movie-info/movie-info.component';
import {StarRatingComponent} from './components/star-rating/star-rating.component';
import {MovieDateFormatPipe} from './pipes/movie-date-format.pipe';
import {MovieDurationPipe} from './pipes/movie-duration.pipe';
import {MovieGenresPipe} from './pipes/movie-genres.pipe';
import {UrlSafePipe} from './pipes/url-safe.pipe';
import {MatTooltipModule} from '@angular/material/tooltip';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MatCardModule} from '@angular/material/card';


@NgModule({
  declarations: [
    ActorsCarouselComponent,
    CommentsSectionComponent,
    MovieActorCardComponent,
    MovieInfoComponent,
    StarRatingComponent,
    MovieDateFormatPipe,
    MovieDurationPipe,
    MovieGenresPipe,
    UrlSafePipe
  ],
  imports: [
    CommonModule,
    MatTooltipModule,
    NgbModule,
    MatCardModule
  ]
})
export class MovieInfoModule { }
