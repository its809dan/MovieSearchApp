import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'movieDuration'
})
export class MovieDurationPipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): string {
    if (value) {
      let hours = 0;
      while (value >= 60) {
        hours++;
        value -= 60;
      }
      return hours + 'h ' + value + 'm';
    }
    return 'unknown runtime';
  }

}
