import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'movieDateFormat'
})
export class MovieDateFormatPipe implements PipeTransform {

  transform(value: string, ...args: any[]): string {
    if (value) {
      if (args[0].reformat === true) {
        const [year, month, date] = value.split(args[0].delimiter);
        return month + '/' + date + '/' + year;
      }
      return value.split(args[0].delimiter)[0];
    }
    return 'unknown release date';
  }

}
