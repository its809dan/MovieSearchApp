import { Pipe, PipeTransform } from '@angular/core';
import {Genre} from '../../../models/the-movie-db/genre';

@Pipe({
  name: 'movieGenres'
})
export class MovieGenresPipe implements PipeTransform {

  transform(value: Genre[], ...args: unknown[]): string {
    if (value.length) {
      let result = '';
      for (const genre of value) {
        result += genre.name + ', ';
      }
      return result.substring(0, result.length - 2);
    }
    return 'unknown genres';
  }

}
