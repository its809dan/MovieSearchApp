import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {MovieTrailers} from '../../../models/the-movie-db/movie-trailers';
import {Observable} from 'rxjs';
import {MovieService} from '../../../services/movie.service';

@Injectable({
  providedIn: 'root'
})
export class MovieTrailersResolverService implements Resolve<MovieTrailers> {

  constructor(private moviesService: MovieService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<MovieTrailers> {
    return this.moviesService.getTrailersByMovieId(route.params.id);
  }
}
