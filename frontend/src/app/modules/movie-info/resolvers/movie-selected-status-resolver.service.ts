import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {MovieSelectedStatus} from '../../../models/movie-selected-status';
import {Observable} from 'rxjs';
import {UserMoviesService} from '../../../services/user-movies.service';
import {UserService} from '../../../services/user.service';

@Injectable({
  providedIn: 'root'
})
export class MovieSelectedStatusResolverService implements Resolve<MovieSelectedStatus> {

  constructor(private userService: UserService, private userMoviesService: UserMoviesService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<MovieSelectedStatus> {
    if (this.userService.isUserAuthenticated(this.userService.getAuthenticationToken())) {
      return this.userMoviesService.getMovieSelectedStatus(route.params.id);
    }
  }
}
