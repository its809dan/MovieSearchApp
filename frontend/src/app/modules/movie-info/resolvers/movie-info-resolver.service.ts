import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {MovieInfo} from '../../../models/the-movie-db/movie-info';
import {MovieService} from '../../../services/movie.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MovieInfoResolverService implements Resolve<MovieInfo> {

  constructor(private moviesService: MovieService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<MovieInfo> {
    return this.moviesService.getMovieInfoById(route.params.id);
  }
}
