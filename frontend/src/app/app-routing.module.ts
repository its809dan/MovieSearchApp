import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ActivatedRouteSnapshot, RouterModule, Routes} from '@angular/router';
import {HomePageComponent} from './components/home-page/home-page.component';
import {MoviesCollectionComponent} from './components/movies-collection/movies-collection.component';
import {MoviesCollectionResolverService} from './services/resolvers/movies-collection-resolver.service';
import {MovieSearchResultsComponent} from './components/movie-search-results/movie-search-results.component';
import {LoginComponent} from './components/login/login.component';
import {NotFoundPageComponent} from './components/not-found-page/not-found-page.component';
import {UserPageComponent} from './components/user-page/user-page.component';
import {UserMoviesCollectionComponent} from './components/user-movies-collection/user-movies-collection.component';
import {AuthGuard} from './services/guards/auth.guard';
import {UserSettingsComponent} from './components/user-settings/user-settings.component';
import {MovieInfoComponent} from './modules/movie-info/components/movie-info/movie-info.component';
import {MovieInfoResolverService} from './modules/movie-info/resolvers/movie-info-resolver.service';
import {MovieSelectedStatusResolverService} from './modules/movie-info/resolvers/movie-selected-status-resolver.service';
import {MovieTrailersResolverService} from './modules/movie-info/resolvers/movie-trailers-resolver.service';

const routes: Routes = [
  {path: '', component: HomePageComponent},
  {path: 'home', redirectTo: ''},
  {path: 'movies/:type', component: MoviesCollectionComponent,
    resolve: {moviesCollection: MoviesCollectionResolverService},
    data: {withSearch: false, collectionType: 'compilation'},
    runGuardsAndResolvers: (current: ActivatedRouteSnapshot, future: ActivatedRouteSnapshot) => {
      return current.queryParams.page !== future.queryParams.page;
    }
  },
  {path: 'discover/movies', component: MoviesCollectionComponent,
    resolve: {moviesCollection: MoviesCollectionResolverService},
    data: {withSearch: true, collectionType: 'discover'},
    runGuardsAndResolvers: (current: ActivatedRouteSnapshot, future: ActivatedRouteSnapshot) => {
      return current.queryParams.page !== future.queryParams.page;
    }
  },
  {path: 'movie/:id', component: MovieInfoComponent,
    resolve: {
      movieInfo: MovieInfoResolverService,
      movieSelectedStatus: MovieSelectedStatusResolverService,
      movieTrailers: MovieTrailersResolverService
    }
  },
  {path: 'search', component: MovieSearchResultsComponent},
  {path: 'login', component: LoginComponent},
  {path: 'not-found', component: NotFoundPageComponent},
  {path: 'profile/:username', component: UserPageComponent},
  {path: 'favorites', component: UserMoviesCollectionComponent, canActivate: [AuthGuard], data: {collectionType: 'favorites'}},
  {path: 'watch-later', component: UserMoviesCollectionComponent, canActivate: [AuthGuard], data: {collectionType: 'watchLater'}},
  {path: 'settings', component: UserSettingsComponent, canActivate: [AuthGuard]},
  {path: '**', component: NotFoundPageComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled'
    }),
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
