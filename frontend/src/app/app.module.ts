import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { HeaderComponent } from './components/header/header.component';
import { MoviesListComponent } from './components/movies-list/movies-list.component';
import { LoginComponent } from './components/login/login.component';
import { MoviePosterTitlePipe } from './pipes/movie-poster-title.pipe';
import { MoviesCollectionComponent } from './components/movies-collection/movies-collection.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { MovieSearchResultsComponent } from './components/movie-search-results/movie-search-results.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';
import {MatCardModule} from '@angular/material/card';
import { MovieSearchFiltersComponent } from './components/movie-search-filters/movie-search-filters.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSliderModule} from '@angular/material/slider';
import {Ng5SliderModule} from 'ng5-slider';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { FooterComponent } from './components/footer/footer.component';
import { UserPageComponent } from './components/user-page/user-page.component';
import {JwtInterceptor} from './services/interceptors/jwt.interceptor';
import { UserSettingsComponent } from './components/user-settings/user-settings.component';
import { UserMoviesCollectionComponent } from './components/user-movies-collection/user-movies-collection.component';
import {ErrorInterceptor} from './services/interceptors/error.interceptor';
import {AppRoutingModule} from './app-routing.module';
import {MovieInfoModule} from './modules/movie-info/movie-info.module';
import {MoviePosterComponent} from './components/movie-poster/movie-poster.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MoviesListComponent,
    LoginComponent,
    MoviePosterTitlePipe,
    MoviesCollectionComponent,
    HomePageComponent,
    SearchBarComponent,
    MovieSearchResultsComponent,
    NotFoundPageComponent,
    MovieSearchFiltersComponent,
    FooterComponent,
    UserPageComponent,
    UserSettingsComponent,
    UserMoviesCollectionComponent,
    MoviePosterComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatTooltipModule,
    FormsModule,
    NgbModule,
    MatCardModule,
    MatExpansionModule,
    MatSliderModule,
    Ng5SliderModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    AppRoutingModule,
    MovieInfoModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
