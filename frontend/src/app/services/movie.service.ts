import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {MoviesListTypes} from '../constants/movies-list-types';
import {FilterOptions} from '../models/the-movie-db/filter-options';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  private movieDbUrl = 'https://api.themoviedb.org/3';

  constructor(private http: HttpClient) { }

  getMoviesByType(moviesListType: string, page: number): Observable<any> {
    switch (moviesListType.toUpperCase()) {
      case MoviesListTypes.POPULAR:
        return this.getPopularMovies(page);
      case MoviesListTypes.NOW_PLAYING:
        return this.getNowPlayingMovies(page);
      case MoviesListTypes.UPCOMING:
        return this.getUpcomingMovies(page);
      case MoviesListTypes.TOP_RATED:
        return this.getTopRatedMovies(page);
      default:
        return this.getPopularMovies(page);
    }
  }

  getHomePageMovies(): Observable<any> {
    return this.http.get<any>(`${this.movieDbUrl}/movie/now_playing?api_key=${environment.MOVIE_DB_API_KEY}&page=1`);
  }

  getPopularMovies(page: number): Observable<any> {
    return this.http.get<any>(`${this.movieDbUrl}/movie/popular?api_key=${environment.MOVIE_DB_API_KEY}&page=${page}`);
  }

  getNowPlayingMovies(page: number): Observable<any> {
    return this.http.get<any>(`${this.movieDbUrl}/movie/now_playing?api_key=${environment.MOVIE_DB_API_KEY}&page=${page}`);
  }

  getUpcomingMovies(page: number): Observable<any> {
    return this.http.get<any>(`${this.movieDbUrl}/movie/upcoming?api_key=${environment.MOVIE_DB_API_KEY}&page=${page}`);
  }

  getTopRatedMovies(page: number): Observable<any> {
    return this.http.get<any>(`${this.movieDbUrl}/movie/top_rated?api_key=${environment.MOVIE_DB_API_KEY}&page=${page}`);
  }

  getMovieInfoById(id: number): Observable<any> {
    return this.http.get<any>(`${this.movieDbUrl}/movie/${id}?api_key=${environment.MOVIE_DB_API_KEY}`);
  }

  getActorsByMovieId(id: number): Observable<any> {
    return this.http.get<any>(`${this.movieDbUrl}/movie/${id}/credits?api_key=${environment.MOVIE_DB_API_KEY}`);
  }

  getReviewsByMovieId(id: number): Observable<any> {
    return this.http.get<any>(`${this.movieDbUrl}/movie/${id}/reviews?api_key=${environment.MOVIE_DB_API_KEY}`);
  }

  getTrailersByMovieId(id: number): Observable<any> {
    return this.http.get<any>(`${this.movieDbUrl}/movie/${id}/videos?api_key=${environment.MOVIE_DB_API_KEY}`);
  }

  getImagesByMovieId(id: number): Observable<any> {
    return this.http.get<any>(`${this.movieDbUrl}/movie/${id}/images?api_key=${environment.MOVIE_DB_API_KEY}`);
  }

  getMoviesBySearchQuery(query: string, page: number): Observable<any> {
    return this.http.get<any>(`${this.movieDbUrl}/search/movie?api_key=${environment.MOVIE_DB_API_KEY}&query=${query}&page=${page}`);
  }

  getAllGenres(): Observable<any> {
    return this.http.get<any>(`${this.movieDbUrl}/genre/movie/list?api_key=${environment.MOVIE_DB_API_KEY}`);
  }

  getMoviesBySelectedFilters(selectedFilters: FilterOptions, page: number): Observable<any> {
    const addAnd = (queryString) => {
      if (queryString.charAt(queryString.length - 1) !== '&') {
        queryString += '&';
      }
      return queryString;
    };

    const parseDate = (date: Date) => {
      const year = date.getUTCFullYear().toString();
      let month = date.getUTCMonth().toString();
      if (month.toString().length === 1) {
        month = '0' + month;
      }
      let day = date.getUTCDay().toString();
      if (day.toString().length === 1) {
        day = '0' + day;
      }
      return year + '-' + month + '-' + day;
    };

    let query = `page=${page}`;
    if (selectedFilters) {
      if (selectedFilters.sortType) {
        query = addAnd(query) + `sort_by=${selectedFilters.sortType.code}`;
      } else {
        query = addAnd(query) + 'sort_by=popularity.desc';
      }
      if (selectedFilters.genres.length) {
        let genresIds = '';
        for (const genre of selectedFilters.genres) {
          genresIds += genre.id + ',';
        }
        genresIds = genresIds.substring(0, genresIds.length - 1);
        query = addAnd(query) + `with_genres=${genresIds}`;
      }
      if (selectedFilters.fromReleaseDate) {
        const date = parseDate(selectedFilters.fromReleaseDate);
        query = addAnd(query) + `release_date.gte=${date}`;
      }
      if (selectedFilters.toReleaseDate) {
        const date = parseDate(selectedFilters.toReleaseDate);
        query = addAnd(query) + `release_date.lte=${date}`;
      }
      if (selectedFilters.minUserScore) {
        query = addAnd(query) + `vote_average.gte=${selectedFilters.minUserScore}`;
      }
      if (selectedFilters.maxUserScore) {
        query = addAnd(query) + `vote_average.lte=${selectedFilters.maxUserScore}`;
      }
      if (selectedFilters.minRuntime) {
        query = addAnd(query) + `with_runtime.gte=${selectedFilters.minRuntime}`;
      }
      if (selectedFilters.maxRuntime) {
        query = addAnd(query) + `with_runtime.lte=${selectedFilters.maxRuntime}`;
      }
    }
    return this.http.get<any>(`${this.movieDbUrl}/discover/movie?api_key=${environment.MOVIE_DB_API_KEY}&${query}`);
  }
}
