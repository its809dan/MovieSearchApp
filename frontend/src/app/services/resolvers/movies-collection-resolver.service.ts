import { Injectable } from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {MoviePoster} from '../../models/the-movie-db/movie-poster';
import {Observable} from 'rxjs';
import {MovieService} from '../movie.service';

@Injectable({
  providedIn: 'root'
})
export class MoviesCollectionResolverService implements Resolve<any> {

  constructor(private moviesService: MovieService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const collectionType = route.data.collectionType;
    if (collectionType === 'compilation') {
      if (route.params.type) {
        let page = route.queryParams.page;
        if (!page) {
          page = 1;
        }
        return this.moviesService.getMoviesByType(route.params.type, page);
      }
    } else if (collectionType === 'discover') {
      let page = route.queryParams.page;
      if (!page) {
        page = 1;
      }
      return this.moviesService.getMoviesBySelectedFilters(null, page);
    }
    return undefined;
  }
}
