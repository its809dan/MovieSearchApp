import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {LoginForm} from '../models/login-form';
import {Observable} from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import {SERVICE_URL_BASE} from '../constants/consts';
import {SignupForm} from '../models/signup-form';
import {UserSettings} from '../models/user-settings';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private urlBase = SERVICE_URL_BASE;

  constructor(private http: HttpClient) { }

  signUpUser(signUp: SignupForm): Observable<any> {
    return this.http.post(this.urlBase + '/registration', signUp, {observe: 'response'});
  }

  isUserAuthenticated(token: string): boolean {
    const jwtHelperService = new JwtHelperService();
    try {
      return !jwtHelperService.isTokenExpired(token);
    } catch (e) {
      return false;
    }
  }

  loginUser(login: LoginForm): Observable<any> {
    return this.http.post(this.urlBase + '/login', login, {observe: 'response'});
  }

  logoutUser(): void {
    localStorage.removeItem('authData');
  }

  getAuthenticatedUsername(): string {
    if (localStorage.getItem('authData') === null) {
      return null;
    }
    return JSON.parse(localStorage.getItem('authData')).username;
  }

  getAuthenticationToken(): string {
    if (localStorage.getItem('authData') === null) {
      return null;
    }
    return JSON.parse(localStorage.getItem('authData')).token;
  }

  getAuthenticationTokenType(): string {
    if (localStorage.getItem('authData') === null) {
      return null;
    }
    return JSON.parse(localStorage.getItem('authData')).type;
  }

  getUserInfo(username: string): Observable<any> {
    return this.http.get(this.urlBase + `/users/${username}`, {observe: 'response'});
  }

  updateUserInfo(userSettings: UserSettings): Observable<any> {
    let birthDate;
    if (userSettings.birthDay && userSettings.birthMonth && userSettings.birthYear) {
      let month = userSettings.birthMonth.toString();
      if (month.length === 1) {
        month = '0' + month;
      }
      birthDate = userSettings.birthYear + '-' + month + '-' + userSettings.birthDay;
    }
    const updateUser = {
      email: userSettings.email,
      birthdate: birthDate,
      password: userSettings.password,
    };
    return this.http.put(this.urlBase + '/profile', updateUser, {observe: 'response'});
  }

  checkIfEmailExists(email: string): Observable<any> {
    return this.http.get(this.urlBase + `/email/status?value=${email}`, {observe: 'response'});
  }

  checkIfUsernameExists(username: string): Observable<any> {
    return this.http.get(this.urlBase + `/username/status?value=${username}`, {observe: 'response'});
  }

  deleteAccount(): Observable<any> {
    return this.http.delete(this.urlBase + `/profile`, {observe: 'response'});
  }
}
