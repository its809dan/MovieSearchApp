import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {MovieInfo} from '../models/the-movie-db/movie-info';
import {SERVICE_URL_BASE} from '../constants/consts';
import {HttpClient} from '@angular/common/http';
import {MovieRequest} from '../models/movie-request';

@Injectable({
  providedIn: 'root'
})
export class UserMoviesService {
  private urlBase = SERVICE_URL_BASE;

  constructor(private http: HttpClient) { }

  static movieInfoTransform(movie: MovieInfo): MovieRequest {
    const movieRequest = new MovieRequest();
    movieRequest.id = movie.id;
    movieRequest.title = movie.title;
    movieRequest.originalTitle = movie.original_title;
    if (movie.poster_path) {
      movieRequest.posterUrl = movie.poster_path;
    } else {
      movieRequest.posterUrl = null;
    }
    movieRequest.duration = movie.runtime;
    movieRequest.rating = movie.vote_average;
    return movieRequest;
  }

  getUserFavorites(): Observable<any> {
    return this.http.get(this.urlBase + '/user-movies/favorites', {observe: 'response'});
  }

  getUserWatchLater(): Observable<any> {
    return this.http.get(this.urlBase + '/user-movies/watch-later', {observe: 'response'});
  }

  getMovieSelectedStatus(movieId: number): Observable<any> {
    return this.http.get(this.urlBase + `/user-movies/${movieId}/status`, {observe: 'response'});
  }

  addMovieToFavorites(movie: MovieInfo): Observable<any> {
    const movieRequest = UserMoviesService.movieInfoTransform(movie);
    return this.http.post(this.urlBase + '/user-movies/favorites', movieRequest, {observe: 'response'});
  }

  deleteMovieFromFavorites(movieId: number): Observable<any> {
    return this.http.delete(this.urlBase + `/user-movies/favorites/${movieId}`, {observe: 'response'});
  }

  addMovieToWatchLater(movie: MovieInfo): Observable<any> {
    const movieRequest = UserMoviesService.movieInfoTransform(movie);
    return this.http.post(this.urlBase + '/user-movies/watch-later', movieRequest, {observe: 'response'});
  }

  deleteMovieFromWatchLater(movieId: number): Observable<any> {
    return this.http.delete(this.urlBase + `/user-movies/watch-later/${movieId}`, {observe: 'response'});
  }
}
