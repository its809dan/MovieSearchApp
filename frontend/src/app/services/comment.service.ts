import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CommentRequest} from '../models/comment-request';
import {SERVICE_URL_BASE} from '../constants/consts';

@Injectable({
  providedIn: 'root'
})
export class CommentService {
  private urlBase = SERVICE_URL_BASE;

  constructor(private http: HttpClient) { }

  getMovieComments(movieId: number): Observable<any> {
    return this.http.get(this.urlBase + `/comments/movies/${movieId}`, {observe: 'response'});
  }

  getMovieCommentsCount(movieId: number): Observable<any> {
    return this.http.get(this.urlBase + `/comments/movies/${movieId}/count`, {observe: 'response'});
  }

  getUserCommentsCount(username: string): Observable<any> {
    return this.http.get(this.urlBase + `/comments/users/${username}/count`, {observe: 'response'});
  }

  addComment(comment: CommentRequest): Observable<any> {
    return this.http.post(this.urlBase + '/comments', comment, {observe: 'response'});
  }

  deleteComment(commentId: number): Observable<any> {
    return this.http.delete(this.urlBase + `/comments/${commentId}`, {observe: 'response'});
  }

  editComment(comment: CommentRequest): Observable<any> {
    return this.http.put(this.urlBase + '/comments', comment, {observe: 'response'});
  }
}
