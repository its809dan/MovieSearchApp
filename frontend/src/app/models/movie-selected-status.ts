export interface MovieSelectedStatus {
  inFavorites: boolean;
  inWatchLater: boolean;
}
