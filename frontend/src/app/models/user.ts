export interface User {
  username: string;
  email: string;
  birthDate: string;
}
