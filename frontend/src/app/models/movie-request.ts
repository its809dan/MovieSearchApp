export class MovieRequest {
  id: number;
  title: string;
  originalTitle: string;
  posterUrl: string;
  duration: number;
  rating: number;
}
