export class UserSettings {
  email: string;
  birthDay: number;
  birthMonth: number;
  birthYear: number;
  password: string;
}
