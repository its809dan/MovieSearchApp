export interface UserCollectionMovie {
  id: number;
  title: string;
  originalTitle: string;
  posterUrl: string;
  rating: number;
  duration: number;
}
