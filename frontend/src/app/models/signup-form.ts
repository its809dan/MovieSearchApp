export interface SignupForm {
  email: string;
  username: string;
  password: string;
}
