import {MovieRequest} from './movie-request';

export class CommentRequest {
  id: number;
  content: string;
  movie: MovieRequest;
}
