export interface Comment {
  id: number;
  content: string;
  publishDateTime: string;
  user: string;
}
