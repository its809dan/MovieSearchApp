export interface ActorCard {
  character: string;
  id: number;
  name: string;
  profile_path: string;
}
