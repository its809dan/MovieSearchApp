export interface MovieReview {
  author: string;
  content: string;
  paragraphs: string[];
  id: string;
  url: string;
}
