export interface MovieTrailers {
  mainTrailerLink: string;
  allTrailersLinks: string[];
}
