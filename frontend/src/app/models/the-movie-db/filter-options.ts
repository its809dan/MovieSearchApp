import {MoviesSorting} from './movies-sorting';
import {Genre} from './genre';

export class FilterOptions {
  sortType: MoviesSorting;
  fromReleaseDate: Date;
  toReleaseDate: Date;
  genres: Genre[] = [];
  minUserScore: number;
  maxUserScore: number;
  minRuntime: number;
  maxRuntime: number;
}
