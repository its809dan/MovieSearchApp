export class MoviePoster {
  id: number;
  poster_path: string;
  title: string;
}
