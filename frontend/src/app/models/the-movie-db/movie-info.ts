import {Genre} from './genre';

export interface MovieInfo {
  id: number;
  adult: boolean;
  budget: number;
  genres: Genre[];
  original_title: string;
  poster_path: string;
  release_date: string;
  status: string;
  title: string;
  overview: string;
  vote_average: number;
  runtime: number;
  revenue: number;
}
