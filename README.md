# movie-search-application

This is the MovieSearchApp. It allows users to view full info about movies, get movies compilations, register, add movies to favorites and watch list, leave comments and much more.

Backend technology stack: Spring Boot, Spring Web, Spring Data JPA, Spring Security, MySQL, Maven.
Frontend technology stack: Angular, HTML, CSS
