package com.moviesearch.mainservice.service;

import com.moviesearch.mainservice.dto.comment.CommentRequest;
import com.moviesearch.mainservice.exception.UserNotFoundException;
import com.moviesearch.mainservice.model.Comment;
import com.moviesearch.mainservice.model.Movie;
import com.moviesearch.mainservice.model.User;
import com.moviesearch.mainservice.repository.CommentRepository;
import com.moviesearch.mainservice.service.implementation.CommentServiceImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTests {

    @Mock
    private CommentRepository commentRepository;

    @Mock
    private UserService userService;

    @Mock
    private UserMovieService userMovieService;

    @InjectMocks
    private CommentServiceImpl commentService;

    private static User user;
    private static List<Comment> comments;
    private static CommentRequest commentRequest;

    @BeforeClass
    public static void prepareData() {
        user = User.builder().username("user").build();

        Comment comment1 = Comment.builder()
                .id(1L)
                .content("Comment 1")
                .user(user)
                .build();
        User user1 = User.builder().username("user1").build();
        Comment comment2 = Comment.builder()
                .id(2L)
                .content("Comment 2")
                .user(user1)
                .build();
        comments = Arrays.asList(comment1, comment2);

        Movie movie = Movie.builder().title("Movie").build();
        commentRequest = CommentRequest.builder()
                .id(1L)
                .content("Comment")
                .movie(movie)
                .build();
    }

    @Test
    public void getMovieComments_validMovieId_shouldReturnCommentsToCertainMovie() {
        Long movieId = 100L;
        when(commentRepository.findByMovie_Id(movieId)).thenReturn(comments);

        List<Comment> actualComments = commentService.getMovieComments(movieId);

        assertEquals(2, actualComments.size());
        assertEquals(actualComments.get(0).getContent(), comments.get(0).getContent());
        assertEquals(actualComments.get(1).getContent(), comments.get(1).getContent());
    }

    @Test
    public void getMovieComments_nullMovieId_shouldThrowException() {
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> commentService.getMovieComments(null));
        assertNotNull(exception.getMessage());
    }

    @Test
    public void getMovieCommentsCount_validMovieId_shouldReturnCommentsCountToCertainMovie() {
        Long movieId = 100L;
        int expectedCount = 10;
        when(commentRepository.countByMovie_Id(movieId)).thenReturn(expectedCount);

        int actualCount = commentService.getMovieCommentsCount(movieId);

        assertEquals(expectedCount, actualCount);
    }

    @Test
    public void getMovieCommentsCount_nullMovieId_shouldThrowException() {
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> commentService.getMovieCommentsCount(null));
        assertNotNull(exception.getMessage());
    }

    @Test
    public void getUserCommentsByUsername_validUsername_shouldReturnCertainUserComments() {
        String username = "username";
        when(commentRepository.findByUser_Username(username)).thenReturn(comments);

        List<Comment> actualComments = commentService.getUserCommentsByUsername(username);

        assertEquals(2, actualComments.size());
        assertEquals(actualComments.get(0).getContent(), comments.get(0).getContent());
        assertEquals(actualComments.get(1).getContent(), comments.get(1).getContent());
    }

    @Test
    public void getUserCommentsByUsername_nullUsername_shouldReturnCertainUserComments() {
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> commentService.getUserCommentsByUsername(null));
        assertNotNull(exception.getMessage());
    }

    @Test
    public void getUserCommentsCountByUsername_validUsername_shouldReturnCertainUserComments() {
        String username = "username";
        int expectedCount = 10;
        when(commentRepository.countByUser_Username(username)).thenReturn(expectedCount);

        int actualCount = commentService.getUserCommentsCountByUsername(username);

        assertEquals(expectedCount, actualCount);
    }

    @Test
    public void getUserCommentsCountByUsername_nullUsername_shouldReturnCertainUserComments() {
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> commentService.getUserCommentsCountByUsername(null));
        assertNotNull(exception.getMessage());
    }

    @Test
    public void createComment_validArguments_shouldSaveCommentAndReturnIt() {
        String username = user.getUsername();
        Comment expectedComment = Comment.builder()
                .content(commentRequest.getContent())
                .user(user)
                .movie(commentRequest.getMovie())
                .publishDateTime(LocalDateTime.now())
                .build();
        when(userMovieService.saveMovie(any(Movie.class))).thenReturn(any());
        when(userService.findUserByUsername(username)).thenReturn(user);
        when(commentRepository.save(any(Comment.class))).thenReturn(expectedComment);

        Comment actualComment = commentService.createComment(commentRequest, username);

        assertEquals(expectedComment.getContent(), actualComment.getContent());
        assertEquals(expectedComment.getMovie().getTitle(), actualComment.getMovie().getTitle());
        assertEquals(user, actualComment.getUser());
        assertEquals(expectedComment.getPublishDateTime(), actualComment.getPublishDateTime());
        verify(commentRepository, times(1)).save(any());
    }

    @Test
    public void createComment_nonExistingUsername_shouldThrowException() {
        String username = user.getUsername();
        when(userService.findUserByUsername(username)).thenReturn(null);

        Exception exception = assertThrows(UserNotFoundException.class,
                () -> commentService.createComment(commentRequest, username));
        assertNotNull(exception.getMessage());
    }

    @Test
    public void createComment_nullCommentRequest_shouldThrowException() {
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> commentService.createComment(null, user.getUsername()));
        assertNotNull(exception.getMessage());
    }

    @Test
    public void createComment_nullUsername_shouldThrowException() {
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> commentService.createComment(commentRequest, null));
        assertNotNull(exception.getMessage());
    }

    @Test
    public void updateComment_validCommentRequest_shouldUpdateComment() {
        Comment commentToUpdate = comments.get(0);
        Comment expectedComment = Comment.builder()
                .id(commentRequest.getId())
                .content(commentRequest.getContent())
                .publishDateTime(LocalDateTime.now())
                .build();
        when(commentRepository.findById(commentRequest.getId())).thenReturn(Optional.of(commentToUpdate));
        when(commentRepository.save(any(Comment.class))).thenReturn(expectedComment);

        Comment actualComment = commentService.updateComment(commentRequest);

        assertEquals(commentToUpdate.getId(), actualComment.getId());
        assertEquals(expectedComment.getContent(), actualComment.getContent());
        assertEquals(expectedComment.getPublishDateTime(), actualComment.getPublishDateTime());
        verify(commentRepository, times(1)).save(commentToUpdate);
    }

    @Test
    public void updateComment_nonExistingId_shouldReturnNull() {
        when(commentRepository.findById(commentRequest.getId())).thenReturn(Optional.empty());

        Comment actualComment = commentService.updateComment(commentRequest);

        assertNull(actualComment);
    }

    @Test
    public void updateComment_nullCommentRequest_shouldThrowException() {
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> commentService.updateComment(null));
        assertNotNull(exception.getMessage());
    }

    @Test
    public void deleteComment_validCommentId_shouldDeleteComment() {
        doNothing().when(commentRepository).deleteById(commentRequest.getId());

        commentService.deleteComment(commentRequest.getId());

        verify(commentRepository, times(1)).deleteById(commentRequest.getId());
    }

    @Test
    public void deleteComment_nullCommentId_shouldThrowException() {
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> commentService.deleteComment(null));
        assertNotNull(exception.getMessage());
    }

    @Test
    public void checkIfUserIsCommentAuthor_authorIsCommentAuthor_shouldReturnTrue() {
        Comment comment = comments.get(0);
        when(commentRepository.findById(comment.getId())).thenReturn(Optional.of(comment));

        boolean actualCheckResult = commentService.checkIfUserIsCommentAuthor(comment.getId(), user.getUsername());

        assertTrue(actualCheckResult);
    }

    @Test
    public void checkIfUserIsCommentAuthor_authorIsNotCommentAuthor_shouldReturnFalse() {
        Comment comment = comments.get(1);
        when(commentRepository.findById(comment.getId())).thenReturn(Optional.of(comment));

        boolean actualCheckResult = commentService.checkIfUserIsCommentAuthor(comment.getId(), user.getUsername());

        assertFalse(actualCheckResult);
    }

    @Test
    public void checkIfUserIsCommentAuthor_nonExistingCommentId_shouldReturnFalse() {
        Long commentId = 10L;
        when(commentRepository.findById(commentId)).thenReturn(Optional.empty());

        boolean actualCheckResult = commentService.checkIfUserIsCommentAuthor(commentId, user.getUsername());

        assertFalse(actualCheckResult);
    }

    @Test
    public void checkIfUserIsCommentAuthor_nullCommentId_shouldThrowException() {
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> commentService.checkIfUserIsCommentAuthor(null, user.getUsername()));
        assertNotNull(exception.getMessage());
    }

    @Test
    public void checkIfUserIsCommentAuthor_nullUsername_shouldThrowException() {
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> commentService.checkIfUserIsCommentAuthor(1L, null));
        assertNotNull(exception.getMessage());
    }
}
