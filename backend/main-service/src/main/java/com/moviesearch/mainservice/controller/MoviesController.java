package com.moviesearch.mainservice.controller;

import com.moviesearch.mainservice.dto.movie.Genre;
import com.moviesearch.mainservice.dto.movie.MovieFullDetails;
import com.moviesearch.mainservice.dto.movie.MoviesCollection;
import com.moviesearch.mainservice.dto.movie.ReviewsCollection;
import com.moviesearch.mainservice.service.MovieInfoService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/movies")
@AllArgsConstructor
public class MoviesController {

    private final MovieInfoService movieInfoService;

    @GetMapping("/{movieId}")
    public MovieFullDetails getMovieDetailsById(@PathVariable Long movieId) {
        return movieInfoService.getMovieDetailsById(movieId);
    }

    @GetMapping("/{movieId}/reviews")
    public ReviewsCollection getMovieReviews(@PathVariable Long movieId,
                                             @RequestParam(defaultValue = "1") Integer page) {
        return movieInfoService.getMovieReviewsById(movieId, page);
    }

    @GetMapping("/genres")
    public List<Genre> getAllGenres() {
        return movieInfoService.getAllGenres();
    }

    @GetMapping("/popular")
    public MoviesCollection getPopularMovies(@RequestParam(defaultValue = "1") Integer page) {
        return movieInfoService.getPopularMovies(page);
    }

    @GetMapping("/home")
    public MoviesCollection getHomePageMovies() {
        return movieInfoService.getHomePageMovies();
    }

    @GetMapping("/now")
    public MoviesCollection getNowPlayingMovies(@RequestParam(defaultValue = "1") Integer page) {
        return movieInfoService.getNowPlayingMovies(page);
    }

    @GetMapping("/upcoming")
    public MoviesCollection getUpcomingMovies(@RequestParam(defaultValue = "1") Integer page) {
        return movieInfoService.getUpcomingMovies(page);
    }

    @GetMapping("/top")
    public MoviesCollection getTopRatedMovies(@RequestParam(defaultValue = "1") Integer page) {
        return movieInfoService.getTopRatedMovies(page);
    }
}
