package com.moviesearch.mainservice.repository;

import com.moviesearch.mainservice.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {

    List<Comment> findByUser_Username(String username);

    Integer countByUser_Username(String username);

    List<Comment> findByMovie_Id(Long movieId);

    Integer countByMovie_Id(Long movieId);

}
