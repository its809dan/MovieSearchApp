package com.moviesearch.mainservice.service;

import com.moviesearch.mainservice.dto.movie.Genre;
import com.moviesearch.mainservice.dto.movie.MovieFullDetails;
import com.moviesearch.mainservice.dto.movie.MoviesCollection;
import com.moviesearch.mainservice.dto.movie.ReviewsCollection;

import java.util.List;

public interface MovieInfoService {

    MovieFullDetails getMovieDetailsById(Long movieId);

    ReviewsCollection getMovieReviewsById(Long movieId, Integer page);

    MoviesCollection getPopularMovies(Integer page);

    MoviesCollection getHomePageMovies();

    MoviesCollection getNowPlayingMovies(Integer page);

    MoviesCollection getUpcomingMovies(Integer page);

    MoviesCollection getTopRatedMovies(Integer page);

    List<Genre> getAllGenres();
}
