package com.moviesearch.mainservice.dto.movie;

import lombok.*;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MoviesCollection {

    private int page;

    private List<MovieBasic> movies;

    private int totalPages;

    private int totalResults;
}
