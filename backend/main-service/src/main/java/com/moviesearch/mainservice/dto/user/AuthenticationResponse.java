package com.moviesearch.mainservice.dto.user;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@Data
@NoArgsConstructor
public class AuthenticationResponse {

    private String token;
    private String username;
    private String type = "Bearer";
    private Collection<? extends GrantedAuthority> authorities;

    public AuthenticationResponse(String token, String username, Collection<? extends GrantedAuthority> authorities) {
        this.token = token;
        this.username = username;
        this.authorities = authorities;
    }
}
