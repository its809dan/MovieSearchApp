package com.moviesearch.mainservice.controller;

import com.moviesearch.mainservice.dto.*;
import com.moviesearch.mainservice.dto.user.LoginDataStatus;
import com.moviesearch.mainservice.dto.user.LoginRequest;
import com.moviesearch.mainservice.dto.user.UserEditRequest;
import com.moviesearch.mainservice.dto.user.UserRequest;
import com.moviesearch.mainservice.model.User;
import com.moviesearch.mainservice.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/registration")
    public ResponseEntity<?> registerUser(@Valid @RequestBody UserRequest user) {
        return new ResponseEntity<>(userService.createUser(user), HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<?> loginUser(@Valid @RequestBody LoginRequest loginRequest) {
        if (userService.checkIfUsernameExists(loginRequest.getUsername())) {
            if (userService.checkCredentials(loginRequest)) {
                return new ResponseEntity<>(userService.loginUser(loginRequest), HttpStatus.OK);
            }
            return ResponseEntity.badRequest().body(new ErrorResponse("Wrong credentials"));
        }
        return ResponseEntity.badRequest().body(new ErrorResponse("User does not exist"));
    }

    @GetMapping("/users/{username}")
    public ResponseEntity<?> getUserProfile(@PathVariable String username) {
        User user = userService.findUserByUsername(username);
        if (user == null) {
            return new ResponseEntity<>(new ErrorResponse("User with such username not found"), HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
    }

    @PutMapping("/profile")
    public ResponseEntity<?> editUserProfile(@Valid @RequestBody UserEditRequest user) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        return ResponseEntity.ok(userService.updateUser(username, user));
    }

    @DeleteMapping("/profile")
    public ResponseEntity<?> deleteAccount() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        userService.deleteUserByUsername(username);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/email/status")
    public ResponseEntity<?> getEmailStatus(@RequestParam String value) {
        return new ResponseEntity<>(new LoginDataStatus(this.userService.checkIfEmailExists(value)), HttpStatus.OK);
    }

    @GetMapping("/username/status")
    public ResponseEntity<?> getUsernameStatus(@RequestParam String value) {
        return new ResponseEntity<>(new LoginDataStatus(this.userService.checkIfUsernameExists(value)), HttpStatus.OK);
    }
}
