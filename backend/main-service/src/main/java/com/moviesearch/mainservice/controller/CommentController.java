package com.moviesearch.mainservice.controller;

import com.moviesearch.mainservice.dto.ErrorResponse;
import com.moviesearch.mainservice.dto.comment.CommentRequest;
import com.moviesearch.mainservice.model.Comment;
import com.moviesearch.mainservice.service.CommentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/comments")
@AllArgsConstructor
public class CommentController {

    private final CommentService commentService;

    @GetMapping("/movies/{movieId}")
    public List<Comment> getMovieComments(@PathVariable Long movieId) {
        return commentService.getMovieComments(movieId);
    }

    @GetMapping("/movies/{movieId}/count")
    public Integer getMovieCommentsCount(@PathVariable Long movieId) {
        return commentService.getMovieCommentsCount(movieId);
    }

    @GetMapping("/users/{username}")
    public List<Comment> getUserComments(@PathVariable String username) {
        return commentService.getUserCommentsByUsername(username);
    }

    @GetMapping("/users/{username}/count")
    public Integer getUserCommentsCount(@PathVariable String username) {
        return commentService.getUserCommentsCountByUsername(username);
    }

    @PostMapping
    public Comment addCommentToMovie(@Valid @RequestBody CommentRequest comment) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        return commentService.createComment(comment, username);
    }

    @PutMapping
    public ResponseEntity<?> editCommentToMovie(@Valid @RequestBody CommentRequest comment) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        if (commentService.checkIfUserIsCommentAuthor(comment.getId(), username)) {
            return new ResponseEntity<>(commentService.updateComment(comment), HttpStatus.CREATED);
        }
        return new ResponseEntity<>(new ErrorResponse("User is not author of the comment"), HttpStatus.FORBIDDEN);
    }

    @DeleteMapping("/{commentId}")
    public ResponseEntity<?> deleteCommentToMovie(@PathVariable Long commentId) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        if (commentService.checkIfUserIsCommentAuthor(commentId, username)) {
            commentService.deleteComment(commentId);
            return ResponseEntity.noContent().build();
        }
        return new ResponseEntity<>(new ErrorResponse("User is not author of the comment"), HttpStatus.FORBIDDEN);
    }
}
