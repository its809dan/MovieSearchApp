package com.moviesearch.mainservice.dto.movie;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Genre {

    private int id;

    private String name;
}
