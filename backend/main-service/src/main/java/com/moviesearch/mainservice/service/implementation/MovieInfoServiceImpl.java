package com.moviesearch.mainservice.service.implementation;

import com.moviesearch.mainservice.dto.movie.Genre;
import com.moviesearch.mainservice.dto.movie.MovieFullDetails;
import com.moviesearch.mainservice.dto.movie.MoviesCollection;
import com.moviesearch.mainservice.dto.movie.ReviewsCollection;
import com.moviesearch.mainservice.service.MovieInfoService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Service
@AllArgsConstructor
public class MovieInfoServiceImpl implements MovieInfoService {

    private static final String MOVIES_SERVICE_BASE = "http://localhost:8081";

    private final RestTemplate restTemplate;

    @Override
    public MovieFullDetails getMovieDetailsById(Long movieId) {
        if (Objects.isNull(movieId)) throw new IllegalArgumentException("Movie id cannot be null");
        String uri = MOVIES_SERVICE_BASE + "/movies/{movieId}";
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("movieId", Long.toString(movieId));
        try {
            return restTemplate.getForObject(uri, MovieFullDetails.class, uriVariables);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public ReviewsCollection getMovieReviewsById(Long movieId, Integer page) {
        if (Objects.isNull(movieId)) throw new IllegalArgumentException("Movie id cannot be null");
        if (Objects.isNull(page)) throw new IllegalArgumentException("Page cannot be null");
        String uri = MOVIES_SERVICE_BASE + "info/movies/{movieId}/reviews";
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("movieId", Long.toString(movieId));
        try {
            return restTemplate.getForObject(uri, ReviewsCollection.class, uriVariables);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public MoviesCollection getPopularMovies(Integer page) {
        if (Objects.isNull(page)) throw new IllegalArgumentException("Page cannot be null");
        String uri = MOVIES_SERVICE_BASE + "/movies/popular";
        try {
            return restTemplate.getForObject(uri, MoviesCollection.class);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public MoviesCollection getHomePageMovies() {
        String uri = MOVIES_SERVICE_BASE + "/movies/home";
        try {
            return restTemplate.getForObject(uri, MoviesCollection.class);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public MoviesCollection getNowPlayingMovies(Integer page) {
        if (Objects.isNull(page)) throw new IllegalArgumentException("Page cannot be null");
        String uri = MOVIES_SERVICE_BASE + "/movies/now";
        try {
            return restTemplate.getForObject(uri, MoviesCollection.class);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public MoviesCollection getUpcomingMovies(Integer page) {
        if (Objects.isNull(page)) throw new IllegalArgumentException("Page cannot be null");
        String uri = MOVIES_SERVICE_BASE + "/movies/upcoming";
        try {
            return restTemplate.getForObject(uri, MoviesCollection.class);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public MoviesCollection getTopRatedMovies(Integer page) {
        if (Objects.isNull(page)) throw new IllegalArgumentException("Page cannot be null");
        String uri = MOVIES_SERVICE_BASE + "/movies/top";
        try {
            return restTemplate.getForObject(uri, MoviesCollection.class);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<Genre> getAllGenres() {
        String uri = MOVIES_SERVICE_BASE + "/info/movies/genres";
        try {
            Genre[] genres = restTemplate.getForObject(uri, Genre[].class);
            if (Objects.isNull(genres)) return null;
            return Arrays.asList(genres);
        } catch (Exception e) {
            return null;
        }
    }
}
