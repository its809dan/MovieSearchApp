package com.moviesearch.mainservice.dto.movie;

import lombok.*;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Credits {

    List<Actor> actors;

    List<CrewMember> crew;
}
