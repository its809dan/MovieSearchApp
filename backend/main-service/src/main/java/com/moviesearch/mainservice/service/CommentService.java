package com.moviesearch.mainservice.service;

import com.moviesearch.mainservice.dto.comment.CommentRequest;
import com.moviesearch.mainservice.exception.UserNotFoundException;
import com.moviesearch.mainservice.model.Comment;

import java.util.List;

public interface CommentService {

    List<Comment> getMovieComments(Long movieId);

    Integer getMovieCommentsCount(Long movieId);

    List<Comment> getUserCommentsByUsername(String username);

    Integer getUserCommentsCountByUsername(String username);

    Comment createComment(CommentRequest commentRequest, String username) throws UserNotFoundException;

    Comment updateComment(CommentRequest commentRequest);

    void deleteComment(Long commentId);

    boolean checkIfUserIsCommentAuthor(Long commentId, String username);
}
