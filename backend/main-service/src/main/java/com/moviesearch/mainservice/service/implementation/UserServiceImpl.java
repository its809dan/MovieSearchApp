package com.moviesearch.mainservice.service.implementation;

import com.moviesearch.mainservice.dto.user.AuthenticationResponse;
import com.moviesearch.mainservice.dto.user.UserEditRequest;
import com.moviesearch.mainservice.dto.user.UserRequest;
import com.moviesearch.mainservice.exception.UserDataTakenException;
import com.moviesearch.mainservice.model.Role;
import com.moviesearch.mainservice.repository.RoleRepository;
import com.moviesearch.mainservice.dto.user.LoginRequest;
import com.moviesearch.mainservice.model.User;
import com.moviesearch.mainservice.repository.UserRepository;
import com.moviesearch.mainservice.security.JwtTokenProvider;
import com.moviesearch.mainservice.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Slf4j
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final JwtTokenProvider tokenProvider;

    @Override
    public boolean checkCredentials(LoginRequest loginRequest) {
        Optional<User> userOptional = userRepository.findByUsername(loginRequest.getUsername());
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            return passwordEncoder.matches(loginRequest.getPassword(), user.getPassword());
        }
        return false;
    }

    @Override
    public AuthenticationResponse loginUser(LoginRequest loginRequest) {
        Optional<User> userOptional = userRepository.findByUsername(loginRequest.getUsername());
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            UsernamePasswordAuthenticationToken authenticationToken =
                    new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            String jwt = tokenProvider.generateToken(SecurityContextHolder.getContext().getAuthentication());
            log.info("User with username {} logged in", loginRequest.getUsername());
            return new AuthenticationResponse(jwt, user.getUsername(), user.getAuthorities());
        }
        log.error("User with username {} not logged in. Reason: user with this username not found",
                loginRequest.getUsername());
        return null;
    }

    @Override
    public User createUser(UserRequest userRequest) throws UserDataTakenException {
        if (checkIfUsernameExists(userRequest.getUsername())) {
            log.error("User not created. Username {} is taken", userRequest.getUsername());
            throw new UserDataTakenException("User with such username already exists");
        }
        if (checkIfEmailExists(userRequest.getEmail())) {
            log.error("User not created. Email {} is taken", userRequest.getEmail());
            throw new UserDataTakenException("User with such email already exists");
        }
        User user = new User();
        Optional<Role> userRoleOptional = roleRepository.findByAuthority("USER");
        user.setUsername(userRequest.getUsername().toLowerCase());
        user.setEmail(userRequest.getEmail().toLowerCase());
        user.setPassword(passwordEncoder.encode(userRequest.getPassword()));
        user.setAuthorities(new HashSet<>(Collections.singletonList(userRoleOptional.get())));
        log.info("User with username {} created", user.getUsername());
        return userRepository.save(user);
    }

    @Override
    public User updateUser(User user) {
        if (!userRepository.existsById(user.getId())) {
            log.error("User with id {} not updated. Reason: user with this id not found", user.getId());
            return null;
        }
        user.setEmail(user.getEmail().toLowerCase());
        user.setUsername(user.getUsername().toLowerCase());
        log.info("User {} updated", user.getUsername());
        return userRepository.save(user);
    }

    @Override
    public User updateUser(UserRequest userRequest) {
        Optional<User> userOptional = userRepository.findById(userRequest.getId());
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            user.setEmail(userRequest.getEmail().toLowerCase());
            user.setPassword(passwordEncoder.encode(userRequest.getPassword()));
            user.setBirthdate(userRequest.getBirthdate());
            log.info("User {} updated", user.getUsername());
            return userRepository.save(user);
        } else {
            log.error("User with id {} not updated. Reason: user with this id not found", userRequest.getId());
            return null;
        }
    }

    @Override
    public User updateUser(String username, UserEditRequest userEditRequest) {
        Optional<User> userOptional = userRepository.findByUsername(username);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            if (userEditRequest.getEmail() != null) {
                if (checkIfEmailExists(userEditRequest.getEmail())) {
                    log.error("User not updated. Email {} is taken", userEditRequest.getEmail());
                    throw new UserDataTakenException("User with such email already exists");
                }
                user.setEmail(userEditRequest.getEmail().toLowerCase());
            }
            if (userEditRequest.getPassword() != null) {
                user.setPassword(passwordEncoder.encode(userEditRequest.getPassword()));
            }
            if (userEditRequest.getBirthdate() != null) {
                user.setBirthdate(userEditRequest.getBirthdate());
            }
            log.info("User {} updated", user.getUsername());
            return userRepository.save(user);
        } else {
            log.error("User with id {} not updated. Reason: user with this username not found", username);
            return null;
        }
    }


    @Override
    @Transactional
    public void deleteUserByUsername(String username) {
        userRepository.deleteByUsername(username.toLowerCase());
        log.info("User with username {} deleted", username);
    }

    @Override
    @Transactional
    public void deleteUserByEmail(String email) {
        userRepository.deleteByEmail(email.toLowerCase());
        log.info("User with email {} deleted", email);
    }

    @Override
    public boolean checkIfUsernameExists(String username) {
        return userRepository.existsByUsername(username.toLowerCase());
    }

    @Override
    public boolean checkIfEmailExists(String email) {
        return userRepository.existsByEmail(email.toLowerCase());
    }

    @Override
    public User findUserByUsername(String username) {
        if (userRepository.findByUsername(username.toLowerCase()).isPresent()) {
            return userRepository.findByUsername(username.toLowerCase()).get();
        }
        return null;
    }

    @Override
    public User findUserById(Long userId) {
        if (userRepository.findById(userId).isPresent()) {
            return userRepository.findById(userId).get();
        }
        return null;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUsername(username.toLowerCase());
        if (user.isPresent()) {
            return user.get();
        } else {
            throw new UsernameNotFoundException("Such username does not exist");
        }
    }
}
