package com.moviesearch.mainservice.service.implementation;

import com.moviesearch.mainservice.exception.UserNotFoundException;
import com.moviesearch.mainservice.model.Movie;
import com.moviesearch.mainservice.model.User;
import com.moviesearch.mainservice.repository.MovieRepository;
import com.moviesearch.mainservice.service.UserMovieService;
import com.moviesearch.mainservice.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class UserMovieServiceImpl implements UserMovieService {

    private final MovieRepository movieRepository;
    private final UserService userService;

    @Override
    public Movie saveMovie(Movie movie) {
        log.info("Movie with id {} saved", movie.getId());
        return movieRepository.save(movie);
    }

    @Override
    public Movie addMovieToFavorites(String username, Movie movie) throws UserNotFoundException {
        saveMovie(movie);
        User foundUser = userService.findUserByUsername(username);
        if (foundUser == null) {
            log.error("Movie with id {} not added to user's (username - {}) favorites. " +
                    "Reason: user with this username not found", movie.getId(), username);
            throw new UserNotFoundException("User does not exist");
        }
        foundUser.addMovieToFavorites(movie);
        userService.updateUser(foundUser);
        log.info("Movie with id {} added to user's (username - {}) favorites", movie.getId(), username);
        return movie;
    }

    @Override
    public Movie addMovieToWatchLater(String username, Movie movie) throws UserNotFoundException {
        saveMovie(movie);
        User foundUser = userService.findUserByUsername(username);
        if (foundUser == null) {
            log.error("Movie with id {} not added to user's (username - {}) watch later. " +
                    "Reason: user with this username not found", movie.getId(), username);
            throw new UserNotFoundException("User does not exist");
        }
        foundUser.addMovieToWatchLater(movie);
        userService.updateUser(foundUser);
        log.info("Movie with id {} added to user's (username - {}) watch later", movie.getId(), username);
        return movie;
    }

    @Override
    public void deleteMovieFromFavorites(String username, Long movieId) throws UserNotFoundException {
        User foundUser = userService.findUserByUsername(username);
        if (foundUser == null) {
            log.error("Movie with id {} not deleted from user's (username - {}) favorites. " +
                    "Reason: user with this username not found", movieId, username);
            throw new UserNotFoundException("User does not exist");
        }
        foundUser.deleteMovieFromFavorites(movieId);
        userService.updateUser(foundUser);
        log.info("Movie with id {} deleted user's (username - {}) favorites", movieId, username);
    }

    @Override
    public void deleteMovieFromWatchLater(String username, Long movieId) throws UserNotFoundException {
        User foundUser = userService.findUserByUsername(username);
        if (foundUser == null) {
            log.error("Movie with id {} not deleted from user's (username - {}) watch later. " +
                    "Reason: user with this username not found", movieId, username);
            throw new UserNotFoundException("User does not exist");
        }
        foundUser.deleteMovieFromWatchLater(movieId);
        userService.updateUser(foundUser);
        log.info("Movie with id {} deleted user's (username - {}) watch later", movieId, username);
    }

    @Override
    public boolean isMovieInFavorites(String username, Long movieId) throws UserNotFoundException {
        User foundUser = userService.findUserByUsername(username);
        if (foundUser == null) {
            throw new UserNotFoundException("User does not exist");
        }
        return foundUser.getFavorites().stream().anyMatch(movie -> movie.getId().equals(movieId));
    }

    @Override
    public boolean isMovieInWatchLater(String username, Long movieId) throws UserNotFoundException {
        User foundUser = userService.findUserByUsername(username);
        if (foundUser == null) {
            throw new UserNotFoundException("User does not exist");
        }
        return foundUser.getWatchLater().stream().anyMatch(movie -> movie.getId().equals(movieId));
    }

    @Override
    public List<Movie> getUserFavorites(String username) throws UserNotFoundException {
        User foundUser = userService.findUserByUsername(username);
        if (foundUser == null) {
            throw new UserNotFoundException("User does not exist");
        }
        return foundUser.getFavorites();
    }

    @Override
    public List<Movie> getUserWatchLater(String username) throws UserNotFoundException {
        User foundUser = userService.findUserByUsername(username);
        if (foundUser == null) {
            throw new UserNotFoundException("User does not exist");
        }
        return foundUser.getWatchLater();
    }
}
