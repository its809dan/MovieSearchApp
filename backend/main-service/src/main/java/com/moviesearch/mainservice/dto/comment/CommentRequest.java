package com.moviesearch.mainservice.dto.comment;

import com.moviesearch.mainservice.model.Movie;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CommentRequest {

    private Long id;

    @NotBlank
    @Size(min = 1, max = 100)
    private String content;

    @NotNull
    private Movie movie;
}
