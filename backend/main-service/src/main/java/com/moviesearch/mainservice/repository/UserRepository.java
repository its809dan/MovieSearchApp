package com.moviesearch.mainservice.repository;

import com.moviesearch.mainservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);

    Optional<User> findByEmail(String email);

    void deleteByUsername(String username);

    void deleteByEmail(String email);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}
