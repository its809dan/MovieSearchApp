package com.moviesearch.mainservice.dto.usermovie;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MovieSelectedStatus {
    private boolean isInFavorites;
    private boolean isInWatchLater;
}