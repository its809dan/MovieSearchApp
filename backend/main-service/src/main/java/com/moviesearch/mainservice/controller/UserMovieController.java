package com.moviesearch.mainservice.controller;

import com.moviesearch.mainservice.dto.ErrorResponse;
import com.moviesearch.mainservice.dto.usermovie.MovieSelectedStatus;
import com.moviesearch.mainservice.model.Movie;
import com.moviesearch.mainservice.service.UserMovieService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/user-movies")
@AllArgsConstructor
public class UserMovieController {

    private final UserMovieService userMovieService;

    @GetMapping("/favorites")
    public List<Movie> getFavorites() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        return userMovieService.getUserFavorites(username);
    }

    @GetMapping("/watch-later")
    public List<Movie> getWatchLater() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        return userMovieService.getUserWatchLater(username);
    }

    @GetMapping("/{movieId}/status")
    public MovieSelectedStatus isMovieInSelected(@PathVariable Long movieId) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        boolean isInFavorites = userMovieService.isMovieInFavorites(username, movieId);
        boolean isInWatchLater = userMovieService.isMovieInWatchLater(username, movieId);
        return new MovieSelectedStatus(isInFavorites, isInWatchLater);
    }

    @PostMapping("/favorites")
    public ResponseEntity<?> addMovieToFavorites(@Valid @RequestBody Movie movie) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        if (userMovieService.isMovieInFavorites(username, movie.getId())) {
            return ResponseEntity.badRequest().body(new ErrorResponse("Movie is already in user's favorites"));
        }
        return new ResponseEntity<>(userMovieService.addMovieToFavorites(username, movie),
                HttpStatus.CREATED);
    }

    @PostMapping("/watch-later")
    public ResponseEntity<?> addMovieToWatchLater(@Valid @RequestBody Movie movie) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        if (userMovieService.isMovieInWatchLater(username, movie.getId())) {
            return ResponseEntity.badRequest().body(new ErrorResponse("Movie is already in user's watch later"));
        }
        return new ResponseEntity<>(userMovieService.addMovieToWatchLater(username, movie),
                HttpStatus.CREATED);
    }

    @DeleteMapping("/favorites/{movieId}")
    public ResponseEntity<?> deleteMovieFromFavorites(@PathVariable Long movieId) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        if (!userMovieService.isMovieInFavorites(username, movieId)) {
            return ResponseEntity.badRequest().body(new ErrorResponse("Movie is not in user's favorites"));
        } else {
            userMovieService.deleteMovieFromFavorites(username, movieId);
            return ResponseEntity.noContent().build();
        }
    }

    @DeleteMapping("/watch-later/{movieId}")
    public ResponseEntity<?> deleteMovieFromWatchLater(@PathVariable Long movieId) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        if (!userMovieService.isMovieInWatchLater(username, movieId)) {
            return ResponseEntity.badRequest().body(new ErrorResponse("Movie is not in user's watch later"));
        } else {
            userMovieService.deleteMovieFromWatchLater(username, movieId);
            return ResponseEntity.noContent().build();
        }
    }
}
