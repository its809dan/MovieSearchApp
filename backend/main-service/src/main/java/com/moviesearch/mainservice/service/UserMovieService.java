package com.moviesearch.mainservice.service;

import com.moviesearch.mainservice.exception.UserNotFoundException;
import com.moviesearch.mainservice.model.Movie;

import java.util.List;

public interface UserMovieService {

    Movie saveMovie(Movie movie);

    Movie addMovieToFavorites(String username, Movie movie) throws UserNotFoundException;

    Movie addMovieToWatchLater(String username, Movie movie) throws UserNotFoundException;

    void deleteMovieFromFavorites(String username, Long movieId) throws UserNotFoundException;

    void deleteMovieFromWatchLater(String username, Long movieId) throws UserNotFoundException;

    boolean isMovieInFavorites(String username, Long movieId) throws UserNotFoundException;

    boolean isMovieInWatchLater(String username, Long movieId) throws UserNotFoundException;

    List<Movie> getUserFavorites(String username) throws UserNotFoundException;

    List<Movie> getUserWatchLater(String username) throws UserNotFoundException;
}
