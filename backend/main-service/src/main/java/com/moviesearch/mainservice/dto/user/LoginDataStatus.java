package com.moviesearch.mainservice.dto.user;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoginDataStatus {
    private boolean taken;
}
