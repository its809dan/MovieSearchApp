package com.moviesearch.mainservice.dto.movie;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Actor {

    private long id;

    private String name;

    private String profilePath;

    private String character;
}
