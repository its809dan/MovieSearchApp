package com.moviesearch.mainservice.service.implementation;

import com.moviesearch.mainservice.dto.comment.CommentRequest;
import com.moviesearch.mainservice.exception.UserNotFoundException;
import com.moviesearch.mainservice.model.Comment;
import com.moviesearch.mainservice.model.User;
import com.moviesearch.mainservice.repository.CommentRepository;
import com.moviesearch.mainservice.service.CommentService;
import com.moviesearch.mainservice.service.UserMovieService;
import com.moviesearch.mainservice.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Slf4j
@AllArgsConstructor
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final UserMovieService userMovieService;
    private final UserService userService;

    @Override
    public List<Comment> getMovieComments(Long movieId) {
        if (Objects.isNull(movieId)) throw new IllegalArgumentException("Movie id cannot be null");
        return commentRepository.findByMovie_Id(movieId);
    }

    @Override
    public Integer getMovieCommentsCount(Long movieId) {
        if (Objects.isNull(movieId)) throw new IllegalArgumentException("Movie id cannot be null");
        return commentRepository.countByMovie_Id(movieId);
    }

    @Override
    public List<Comment> getUserCommentsByUsername(String username) {
        if (Objects.isNull(username)) throw new IllegalArgumentException("Username cannot be null");
        return commentRepository.findByUser_Username(username);
    }

    @Override
    public Integer getUserCommentsCountByUsername(String username) {
        if (Objects.isNull(username)) throw new IllegalArgumentException("Username cannot be null");
        return commentRepository.countByUser_Username(username);
    }

    @Override
    public Comment createComment(CommentRequest commentRequest, String username) throws UserNotFoundException {
        if (Objects.isNull(commentRequest)) throw new IllegalArgumentException("Comment data cannot be null");
        if (Objects.isNull(username)) throw new IllegalArgumentException("Username cannot be null");
        userMovieService.saveMovie(commentRequest.getMovie());
        User user = userService.findUserByUsername(username);
        if (user == null) {
            log.error("Comment not created. Reason: user with username {} does not exist", username);
            throw new UserNotFoundException("User does not exist");
        }
        Comment comment = Comment.builder()
                .content(commentRequest.getContent())
                .publishDateTime(LocalDateTime.now())
                .user(user)
                .movie(commentRequest.getMovie())
                .build();
        log.info("Comment with id {} created. Username - {}", comment.getId(), username);
        return commentRepository.save(comment);
    }

    @Override
    public Comment updateComment(CommentRequest commentRequest) {
        if (Objects.isNull(commentRequest)) throw new IllegalArgumentException("Comment data cannot be null");
        Optional<Comment> commentOptional = commentRepository.findById(commentRequest.getId());
        if (commentOptional.isPresent()) {
            Comment comment = commentOptional.get();
            comment.setContent(commentRequest.getContent());
            comment.setPublishDateTime(LocalDateTime.now());
            log.info("Comment with id {} updated", comment.getId());
            return commentRepository.save(comment);
        }
        log.error("Comment with id {} not updated. Reason: comment with this id not found", commentRequest.getId());
        return null;
    }

    @Override
    public void deleteComment(Long commentId) {
        if (Objects.isNull(commentId)) throw new IllegalArgumentException("Comment id cannot be null");
        commentRepository.deleteById(commentId);
        log.info("Comment with id {} deleted", commentId);
    }

    @Override
    public boolean checkIfUserIsCommentAuthor(Long commentId, String username) {
        if (Objects.isNull(commentId)) throw new IllegalArgumentException("Comment id cannot be null");
        if (Objects.isNull(username)) throw new IllegalArgumentException("Username cannot be null");
        Optional<Comment> commentOptional = commentRepository.findById(commentId);
        if (commentOptional.isPresent()) {
            Comment comment = commentOptional.get();
            return comment.getUser().getUsername().equals(username);
        }
        return false;
    }
}
