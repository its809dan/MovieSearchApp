package com.moviesearch.mainservice.service;

import com.moviesearch.mainservice.dto.user.AuthenticationResponse;
import com.moviesearch.mainservice.dto.user.UserEditRequest;
import com.moviesearch.mainservice.dto.user.UserRequest;
import com.moviesearch.mainservice.exception.UserDataTakenException;
import com.moviesearch.mainservice.model.User;
import com.moviesearch.mainservice.dto.user.LoginRequest;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    boolean checkCredentials(LoginRequest loginRequest);

    AuthenticationResponse loginUser(LoginRequest loginRequest);

    User createUser(UserRequest userRequest) throws UserDataTakenException;

    User updateUser(User user);

    User updateUser(UserRequest userRequest);

    User updateUser(String username, UserEditRequest userEditRequest);

    void deleteUserByUsername(String username);

    void deleteUserByEmail(String email);

    boolean checkIfUsernameExists(String username);

    boolean checkIfEmailExists(String email);

    User findUserByUsername(String username);

    User findUserById(Long userId);
}
