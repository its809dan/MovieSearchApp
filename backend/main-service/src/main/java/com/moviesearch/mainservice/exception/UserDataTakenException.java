package com.moviesearch.mainservice.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UserDataTakenException extends RuntimeException {

    public UserDataTakenException(String message) {
        super(message);
    }
}