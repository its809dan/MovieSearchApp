package com.moviesearch.mainservice.dto.movie;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Image {

    private String filePath;

    private int height;

    private int width;
}
