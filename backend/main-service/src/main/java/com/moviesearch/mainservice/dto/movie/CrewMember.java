package com.moviesearch.mainservice.dto.movie;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CrewMember {

    private long id;

    private String name;

    private String profilePath;

    private String department;

    private String job;
}
