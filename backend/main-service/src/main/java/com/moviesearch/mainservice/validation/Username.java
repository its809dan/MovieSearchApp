package com.moviesearch.mainservice.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy = UsernameValidator.class)
public @interface Username {

    String message() default "Username should contain only latin letters, numbers, underscores and points";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
