package com.moviesearch.mainservice.dto.movie;

import lombok.*;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MovieFullDetails {

    private MovieDetails movie;

    private Credits credits;

    private List<MovieVideo> videos;

    private ImagesCollection images;

    private Review review;
}
