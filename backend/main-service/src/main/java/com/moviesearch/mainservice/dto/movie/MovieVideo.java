package com.moviesearch.mainservice.dto.movie;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MovieVideo {

    private String id;

    private String path;

    private String type;
}
