package com.moviesearch.moviesservice.dto;

import lombok.*;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ReviewsCollectionFromApi {

    private int page;

    private List<ReviewFromApi> results;

    private int total_pages;

    private int total_results;
}
