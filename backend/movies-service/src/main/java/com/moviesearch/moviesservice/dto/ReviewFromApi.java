package com.moviesearch.moviesservice.dto;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ReviewFromApi {

    private ReviewAuthorFromApi author_details;

    private String content;

    private String created_at;

    private String updated_at;
}
