package com.moviesearch.moviesservice.dto;

import lombok.*;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ReviewsCollection {

    private int page;

    private List<Review> results;

    private int totalPages;

    private int totalResults;
}
