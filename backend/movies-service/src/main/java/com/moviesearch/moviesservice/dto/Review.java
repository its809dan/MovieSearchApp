package com.moviesearch.moviesservice.dto;

import lombok.*;

import java.time.LocalDateTime;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Review {

    private ReviewAuthor authorDetails;

    private String content;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;
}
