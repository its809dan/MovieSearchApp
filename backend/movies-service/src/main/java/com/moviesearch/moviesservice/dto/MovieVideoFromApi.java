package com.moviesearch.moviesservice.dto;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MovieVideoFromApi {

    private String id;

    private String key;

    private String type;
}
