package com.moviesearch.moviesservice.dto;

import lombok.*;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MovieBasicFromApi {

    private long id;

    private String poster_path;

    private String title;

    private String original_title;

    private String release_date;

    private boolean adult;

    private int popularity;

    private float vote_average;

    private List<Integer> genre_ids;
}
