package com.moviesearch.moviesservice.dto;

import lombok.*;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ImagesCollectionFromApi {

    private List<ImageFromApi> backdrops;

    private List<ImageFromApi> posters;
}
