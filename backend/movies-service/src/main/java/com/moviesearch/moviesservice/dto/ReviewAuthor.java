package com.moviesearch.moviesservice.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReviewAuthor {

    private String name;

    private String username;

    private String avatarPath;

    private int rating;
}
