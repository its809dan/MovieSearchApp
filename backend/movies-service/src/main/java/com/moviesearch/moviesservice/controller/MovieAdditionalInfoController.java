package com.moviesearch.moviesservice.controller;

import com.moviesearch.moviesservice.dto.ReviewsCollection;
import com.moviesearch.moviesservice.dto.Genre;
import com.moviesearch.moviesservice.service.MovieInfoService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/info/movies")
@AllArgsConstructor
public class MovieAdditionalInfoController {

    private final MovieInfoService movieInfoService;

    @GetMapping("/{movieId}/reviews")
    public ReviewsCollection getMovieReviews(@PathVariable Long movieId,
                                             @RequestParam(defaultValue = "1") Integer page) {
        return movieInfoService.getReviewsByMovieId(movieId, page);
    }

    @GetMapping("/genres")
    public List<Genre> getAllGenres() {
        return movieInfoService.getAllGenres();
    }
}
