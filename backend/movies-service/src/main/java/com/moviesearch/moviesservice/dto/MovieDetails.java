package com.moviesearch.moviesservice.dto;

import lombok.*;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MovieDetails {

    private long id;

    private String posterPath;

    private String title;

    private String originalTitle;

    private String releaseDate;

    private String status;

    private int runtime;

    private boolean adult;

    private String overview;

    private int popularity;

    private float voteAverage;

    private int budget;

    private int revenue;

    private List<Genre> genres;
}
