package com.moviesearch.moviesservice.service.implementation;

import com.moviesearch.moviesservice.dto.*;
import com.moviesearch.moviesservice.dto.Genre;
import com.moviesearch.moviesservice.service.MovieInfoService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class MovieInfoServiceImpl implements MovieInfoService {

    private static final String MOVIE_DB_LINK_BASE = "https://api.themoviedb.org/3";
    private static final String MOVIE_DB_MOVIE_LINK_BASE = MOVIE_DB_LINK_BASE + "/movie";
    private static final String MOVIE_DB_GENRES_BASE = MOVIE_DB_LINK_BASE + "/genre/movie/list";
    private static final String MOVIE_DB_REVIEW_AUTHOR_PROFILE_PATH = "https://www.themoviedb.org/t/p/w150_and_h150_face";
    private static final String MOVIE_DB_CREDITS_MEMBER_PROFILE_PATH = "https://image.tmdb.org/t/p/original";
    private static final String MOVIE_DB_VIDEOS_PATH = "https://www.youtube.com/embed/";
    private static final String MOVIE_DB_IMAGES_PATH = "https://image.tmdb.org/t/p/w533_and_h300_bestv2";
    private static final String API_KEY = "f76e6d5358d21b7e221a0298b908823c";

    private final RestTemplate restTemplate;

    @Override
    public Credits getCreditsByMovieId(Long movieId) {
        if (Objects.isNull(movieId)) throw new IllegalArgumentException("Movie id cannot be null");
        String uri = MOVIE_DB_MOVIE_LINK_BASE + "/{movieId}/credits?api_key={apiKey}";
        Map<String, String> uriVariables = getUriVariablesForMovie(movieId, 0);
        try {
            CreditsFromApi credits = restTemplate.getForObject(uri, CreditsFromApi.class, uriVariables);
            return convertCreditsFromApi(credits);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public ReviewsCollection getReviewsByMovieId(Long movieId, Integer page) {
        if (Objects.isNull(movieId)) throw new IllegalArgumentException("Movie id cannot be null");
        if (Objects.isNull(page)) throw new IllegalArgumentException("Page cannot be null");
        String uri = MOVIE_DB_MOVIE_LINK_BASE + "/{movieId}/reviews?api_key={apiKey}&page={page}";
        Map<String, String> uriVariables = getUriVariablesForMovie(movieId, page);
        try {
            ReviewsCollectionFromApi reviewsCollectionFromApi = restTemplate.getForObject(uri, ReviewsCollectionFromApi.class, uriVariables);
            return convertReviewsCollectionFromApi(reviewsCollectionFromApi);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<MovieVideo> getVideosByMovieId(Long movieId) {
        if (Objects.isNull(movieId)) throw new IllegalArgumentException("Movie id cannot be null");
        String uri = MOVIE_DB_MOVIE_LINK_BASE + "/{movieId}/videos?api_key={apiKey}";
        Map<String, String> uriVariables = getUriVariablesForMovie(movieId, 0);
        try {
            MovieVideosCollectionFromApi videosCollectionFromApi = restTemplate.getForObject(uri, MovieVideosCollectionFromApi.class, uriVariables);
            if (videosCollectionFromApi == null) return null;
            return convertMovieVideosCollectionFromApi(videosCollectionFromApi);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public ImagesCollection getImagesByMovieId(Long movieId) {
        if (Objects.isNull(movieId)) throw new IllegalArgumentException("Movie id cannot be null");
        String uri = MOVIE_DB_MOVIE_LINK_BASE + "/{movieId}/images?api_key={apiKey}";
        Map<String, String> uriVariables = getUriVariablesForMovie(movieId, 0);
        try {
            ImagesCollectionFromApi imagesCollectionFromApi = restTemplate.getForObject(uri, ImagesCollectionFromApi.class, uriVariables);
            return convertImagesCollectionFromApi(imagesCollectionFromApi);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<Genre> getAllGenres() {
        String uri = MOVIE_DB_GENRES_BASE + "?api_key=" + API_KEY;
        try {
            GenresList genresList = restTemplate.getForObject(uri, GenresList.class);
            if (Objects.isNull(genresList)) return null;
            return genresList.getGenres();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Genre getGenreById(Integer genreId) {
        if (Objects.isNull(genreId)) throw new IllegalArgumentException("Genre id cannot be null");
        List<Genre> allGenres = getAllGenres();
        if (Objects.isNull(allGenres)) return null;
        return allGenres.stream()
                .filter(genre -> genre.getId() == genreId)
                .findFirst()
                .orElse(null);
    }

    private Map<String, String> getUriVariablesForMovie(long movieId, int page) {
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("movieId", Long.toString(movieId));
        uriVariables.put("apiKey", API_KEY);
        if (page > 0) uriVariables.put("page", Integer.toString(page));
        return uriVariables;
    }

    private Credits convertCreditsFromApi(CreditsFromApi creditsFromApi) {
        ModelMapper modelMapper = new ModelMapper();
        Credits credits = modelMapper.map(creditsFromApi, Credits.class);
        credits.setActors(creditsFromApi.getCast().stream().map(this::convertActorFromApi).collect(Collectors.toList()));
        credits.setCrew(creditsFromApi.getCrew().stream().map(this::convertCrewMemberFromApi).collect(Collectors.toList()));
        return credits;
    }

    private Actor convertActorFromApi(ActorFromApi actorFromApi) {
        ModelMapper modelMapper = new ModelMapper();
        Actor actor = modelMapper.map(actorFromApi, Actor.class);
        if (actorFromApi.getProfile_path() != null) {
            actor.setProfilePath(MOVIE_DB_CREDITS_MEMBER_PROFILE_PATH + actorFromApi.getProfile_path());
        }
        return actor;
    }

    private CrewMember convertCrewMemberFromApi(CrewMemberFromApi crewMemberFromApi) {
        ModelMapper modelMapper = new ModelMapper();
        CrewMember crewMember = modelMapper.map(crewMemberFromApi, CrewMember.class);
        if (crewMemberFromApi.getProfile_path() != null) {
            crewMember.setProfilePath(MOVIE_DB_CREDITS_MEMBER_PROFILE_PATH + crewMemberFromApi.getProfile_path());
        }
        return crewMember;
    }

    private ReviewsCollection convertReviewsCollectionFromApi(ReviewsCollectionFromApi reviewsCollectionFromApi) {
        ModelMapper modelMapper = new ModelMapper();
        ReviewsCollection reviewsCollection = modelMapper.map(reviewsCollectionFromApi, ReviewsCollection.class);
        reviewsCollection.setTotalPages(reviewsCollectionFromApi.getTotal_pages());
        reviewsCollection.setTotalResults(reviewsCollectionFromApi.getTotal_results());
        reviewsCollection.setResults(reviewsCollectionFromApi.getResults().stream()
                .map(this::convertReviewFromApi).collect(Collectors.toList()));
        return reviewsCollection;
    }

    private Review convertReviewFromApi(ReviewFromApi reviewFromApi) {
        ModelMapper modelMapper = new ModelMapper();
        Review review = modelMapper.map(reviewFromApi, Review.class);
        review.setAuthorDetails(convertReviewAuthorFromApi(reviewFromApi.getAuthor_details()));
        String createdDate = reviewFromApi.getCreated_at().substring(0, reviewFromApi.getCreated_at().length() - 1);
        String updatedDate = reviewFromApi.getUpdated_at().substring(0, reviewFromApi.getCreated_at().length() - 1);
        review.setCreatedAt(LocalDateTime.parse(createdDate));
        review.setUpdatedAt(LocalDateTime.parse(updatedDate));
        return review;
    }

    private ReviewAuthor convertReviewAuthorFromApi(ReviewAuthorFromApi reviewAuthorFromApi) {
        ModelMapper modelMapper = new ModelMapper();
        ReviewAuthor reviewAuthor = modelMapper.map(reviewAuthorFromApi, ReviewAuthor.class);
        if (reviewAuthorFromApi.getAvatar_path() != null) {
            reviewAuthor.setAvatarPath(MOVIE_DB_REVIEW_AUTHOR_PROFILE_PATH + reviewAuthorFromApi.getAvatar_path());
        }
        return reviewAuthor;
    }

    private List<MovieVideo> convertMovieVideosCollectionFromApi(MovieVideosCollectionFromApi videosCollectionFromApi) {
        return videosCollectionFromApi.getResults().stream()
                .map(this::convertMovieVideoFromApi).collect(Collectors.toList());
    }

    private MovieVideo convertMovieVideoFromApi(MovieVideoFromApi movieVideoFromApi) {
        ModelMapper modelMapper = new ModelMapper();
        MovieVideo movieVideo = modelMapper.map(movieVideoFromApi, MovieVideo.class);
        movieVideo.setPath(MOVIE_DB_VIDEOS_PATH + movieVideoFromApi.getKey());
        return movieVideo;
    }

    private ImagesCollection convertImagesCollectionFromApi(ImagesCollectionFromApi imagesCollectionFromApi) {
        ModelMapper modelMapper = new ModelMapper();
        ImagesCollection imagesCollection = modelMapper.map(imagesCollectionFromApi, ImagesCollection.class);
        imagesCollection.setBackdrops(imagesCollectionFromApi.getBackdrops().stream()
                .map(this::convertImageFromApi).collect(Collectors.toList()));
        imagesCollection.setPosters(imagesCollectionFromApi.getPosters().stream()
                .map(this::convertImageFromApi).collect(Collectors.toList()));
        return imagesCollection;
    }

    private Image convertImageFromApi(ImageFromApi imageFromApi) {
        ModelMapper modelMapper = new ModelMapper();
        Image image = modelMapper.map(imageFromApi, Image.class);
        image.setFilePath(MOVIE_DB_IMAGES_PATH + imageFromApi.getFile_path());
        return image;
    }
}
