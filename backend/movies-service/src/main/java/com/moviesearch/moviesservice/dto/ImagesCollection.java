package com.moviesearch.moviesservice.dto;

import lombok.*;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ImagesCollection {

    private List<Image> backdrops;

    private List<Image> posters;
}
