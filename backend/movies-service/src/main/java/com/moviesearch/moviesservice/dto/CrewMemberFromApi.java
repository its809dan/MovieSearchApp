package com.moviesearch.moviesservice.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CrewMemberFromApi {

    private long id;

    private String name;

    private String profile_path;

    private String department;

    private String job;
}
