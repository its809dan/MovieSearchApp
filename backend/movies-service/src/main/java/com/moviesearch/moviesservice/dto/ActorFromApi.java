package com.moviesearch.moviesservice.dto;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ActorFromApi {

    private long id;

    private String name;

    private String profile_path;

    private String character;
}
