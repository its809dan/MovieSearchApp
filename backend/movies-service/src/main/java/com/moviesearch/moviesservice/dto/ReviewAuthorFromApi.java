package com.moviesearch.moviesservice.dto;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ReviewAuthorFromApi {

    private String name;

    private String username;

    private String avatar_path;

    private Integer rating;
}
