package com.moviesearch.moviesservice.dto;

import lombok.*;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MoviesCollectionFromApi {

    private int page;

    private List<MovieBasicFromApi> results;

    private int total_pages;

    private int total_results;
}
