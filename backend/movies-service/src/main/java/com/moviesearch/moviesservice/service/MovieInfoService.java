package com.moviesearch.moviesservice.service;

import com.moviesearch.moviesservice.dto.Credits;
import com.moviesearch.moviesservice.dto.ImagesCollection;
import com.moviesearch.moviesservice.dto.MovieVideo;
import com.moviesearch.moviesservice.dto.ReviewsCollection;
import com.moviesearch.moviesservice.dto.Genre;

import java.util.List;

public interface MovieInfoService {

    Credits getCreditsByMovieId(Long movieId);

    ReviewsCollection getReviewsByMovieId(Long movieId, Integer page);

    List<MovieVideo> getVideosByMovieId(Long movieId);

    ImagesCollection getImagesByMovieId(Long movieId);

    List<Genre> getAllGenres();

    Genre getGenreById(Integer genreId);
}
