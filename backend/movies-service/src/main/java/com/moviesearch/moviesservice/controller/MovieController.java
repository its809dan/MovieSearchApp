package com.moviesearch.moviesservice.controller;

import com.moviesearch.moviesservice.dto.MovieFullDetails;
import com.moviesearch.moviesservice.dto.MoviesCollection;
import com.moviesearch.moviesservice.service.MovieFetcherService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/movies")
@AllArgsConstructor
public class MovieController {

    private final MovieFetcherService movieFetcherService;

    @GetMapping("/{movieId}")
    public MovieFullDetails getMovie(@PathVariable Long movieId) {
        return movieFetcherService.getMovieFullDetailsById(movieId);
    }

    @GetMapping("/popular")
    public MoviesCollection getPopularMovies(@RequestParam(defaultValue = "1") Integer page) {
        return movieFetcherService.getPopularMovies(page);
    }

    @GetMapping("/home")
    public MoviesCollection getHomePageMovies() {
        return movieFetcherService.getHomePageMovies();
    }

    @GetMapping("/now")
    public MoviesCollection getNowPlayingMovies(@RequestParam(defaultValue = "1") Integer page) {
        return movieFetcherService.getNowPlayingMovies(page);
    }

    @GetMapping("/upcoming")
    public MoviesCollection getUpcomingMovies(@RequestParam(defaultValue = "1") Integer page) {
        return movieFetcherService.getUpcomingMovies(page);
    }

    @GetMapping("/top")
    public MoviesCollection getTopRatedMovies(@RequestParam(defaultValue = "1") Integer page) {
        return movieFetcherService.getTopRatedMovies(page);
    }
}
