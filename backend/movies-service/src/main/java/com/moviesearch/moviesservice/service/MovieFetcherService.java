package com.moviesearch.moviesservice.service;

import com.moviesearch.moviesservice.dto.MovieFullDetails;
import com.moviesearch.moviesservice.dto.MoviesCollection;

public interface MovieFetcherService {

    MovieFullDetails getMovieFullDetailsById(Long movieId);

    MoviesCollection getPopularMovies(Integer page);

    MoviesCollection getHomePageMovies();

    MoviesCollection getNowPlayingMovies(Integer page);

    MoviesCollection getUpcomingMovies(Integer page);

    MoviesCollection getTopRatedMovies(Integer page);
}
