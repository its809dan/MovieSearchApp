package com.moviesearch.moviesservice.service.implementation;

import com.moviesearch.moviesservice.dto.*;
import com.moviesearch.moviesservice.dto.Genre;
import com.moviesearch.moviesservice.dto.MovieBasic;
import com.moviesearch.moviesservice.service.MovieFetcherService;
import com.moviesearch.moviesservice.service.MovieInfoService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class MovieFetcherServiceImpl implements MovieFetcherService {

    private static final String MOVIE_DB_POSTER_URL_BASE = "https://image.tmdb.org/t/p/w600_and_h900_bestv2";
    private static final String MOVIE_DB_LINK_BASE = "https://api.themoviedb.org/3";
    private static final String MOVIE_DB_MOVIE_LINK_BASE = MOVIE_DB_LINK_BASE + "/movie";
    private static final int HOME_PAGE_MOVIES_NUMBER = 8;
    private static final String API_KEY = "f76e6d5358d21b7e221a0298b908823c";

    private final MovieInfoService movieInfoService;
    private final RestTemplate restTemplate;

    @Override
    public MovieFullDetails getMovieFullDetailsById(Long movieId) {
        if (Objects.isNull(movieId)) throw new IllegalArgumentException("Movie id cannot be null");
        String uri = MOVIE_DB_MOVIE_LINK_BASE + "/{movieId}?api_key={apiKey}";
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("movieId", Long.toString(movieId));
        uriVariables.put("apiKey", API_KEY);
        try {
            MovieDetailsFromApi movieDetails = restTemplate.getForObject(uri, MovieDetailsFromApi.class, uriVariables);
            MovieFullDetails movieFullDetails = getMovieAdditionalInfo(movieId);
            movieFullDetails.setMovie(convertMovieDetailsFromApi(movieDetails));
            return movieFullDetails;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public MoviesCollection getPopularMovies(Integer page) {
        return getMoviesCollection("popular", page);
    }

    @Override
    public MoviesCollection getHomePageMovies() {
        MoviesCollection homePageMovies = getPopularMovies(1);
        if (Objects.isNull(homePageMovies)) return null;
        homePageMovies.setMovies(homePageMovies.getMovies().stream().limit(HOME_PAGE_MOVIES_NUMBER).collect(Collectors.toList()));
        return homePageMovies;
    }

    @Override
    public MoviesCollection getNowPlayingMovies(Integer page) {
        return getMoviesCollection("now_playing", page);
    }

    @Override
    public MoviesCollection getUpcomingMovies(Integer page) {
        return getMoviesCollection("upcoming", page);
    }

    @Override
    public MoviesCollection getTopRatedMovies(Integer page) {
        return getMoviesCollection("top_rated", page);
    }

    private MovieDetails convertMovieDetailsFromApi(MovieDetailsFromApi movieDetails) {
        ModelMapper modelMapper = new ModelMapper();
        MovieDetails movie = modelMapper.map(movieDetails, MovieDetails.class);
        movie.setPosterPath(MOVIE_DB_POSTER_URL_BASE + movieDetails.getPoster_path());
        movie.setOriginalTitle(movieDetails.getOriginal_title());
        movie.setReleaseDate(movieDetails.getRelease_date());
        movie.setVoteAverage(movieDetails.getVote_average());
        return movie;
    }

    private MoviesCollection getMoviesCollection(String collectionType, Integer page) {
        if (Objects.isNull(page)) throw new IllegalArgumentException("Page cannot be null");
        String uri = MOVIE_DB_MOVIE_LINK_BASE + "/{collectionType}?page={page}&api_key={apiKey}";
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("collectionType", collectionType);
        uriVariables.put("page", Integer.toString(page));
        uriVariables.put("apiKey", API_KEY);
        try {
            MoviesCollectionFromApi moviesFromApi = restTemplate.getForObject(uri, MoviesCollectionFromApi.class, uriVariables);
            return convertMoviesCollectionFromApi(moviesFromApi);
        } catch (Exception e) {
            return null;
        }
    }

    private MoviesCollection convertMoviesCollectionFromApi(MoviesCollectionFromApi moviesCollectionFromApi) {
        ModelMapper modelMapper = new ModelMapper();
        MoviesCollection moviesCollection = modelMapper.map(moviesCollectionFromApi, MoviesCollection.class);
        moviesCollection.setTotalPages(moviesCollectionFromApi.getTotal_pages());
        moviesCollection.setTotalResults(moviesCollectionFromApi.getTotal_results());
        List<MovieBasic> movies = moviesCollectionFromApi.getResults().stream().map(this::convertMovieBasicFromApi).collect(Collectors.toList());
        moviesCollection.setMovies(movies);
        return moviesCollection;
    }

    private MovieBasic convertMovieBasicFromApi(MovieBasicFromApi movieBasicFromApi) {
        ModelMapper modelMapper = new ModelMapper();
        MovieBasic movieBasic = modelMapper.map(movieBasicFromApi, MovieBasic.class);
        if (movieBasicFromApi.getPoster_path() != null) {
            movieBasic.setPosterPath(MOVIE_DB_POSTER_URL_BASE + movieBasicFromApi.getPoster_path());
        }
        movieBasic.setOriginalTitle(movieBasicFromApi.getOriginal_title());
        movieBasic.setReleaseDate(LocalDate.parse(movieBasicFromApi.getRelease_date()));
        movieBasic.setVoteAverage(movieBasicFromApi.getVote_average());
        List<Genre> genres = movieBasicFromApi.getGenre_ids().stream().map(movieInfoService::getGenreById).collect(Collectors.toList());
        movieBasic.setGenres(genres);
        return movieBasic;
    }

    private MovieFullDetails getMovieAdditionalInfo(long movieId) {
        MovieFullDetails movieFullDetails = MovieFullDetails.builder()
                .credits(movieInfoService.getCreditsByMovieId(movieId))
                .videos(movieInfoService.getVideosByMovieId(movieId))
                .images(movieInfoService.getImagesByMovieId(movieId))
                .build();
        ReviewsCollection reviewsCollection = movieInfoService.getReviewsByMovieId(movieId, 1);
        if (reviewsCollection != null && reviewsCollection.getResults() != null && reviewsCollection.getResults().size() != 0) {
            movieFullDetails.setReview(reviewsCollection.getResults().get(0));
        }
        return movieFullDetails;
    }
}
