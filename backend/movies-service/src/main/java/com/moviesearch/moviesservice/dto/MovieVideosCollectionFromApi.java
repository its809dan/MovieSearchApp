package com.moviesearch.moviesservice.dto;

import lombok.*;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MovieVideosCollectionFromApi {

    private long id;

    private List<MovieVideoFromApi> results;
}
