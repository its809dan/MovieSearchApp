package com.moviesearch.moviesservice.dto;

import lombok.*;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MovieDetailsFromApi {

    private long id;

    private String poster_path;

    private String title;

    private String original_title;

    private String release_date;

    private String status;

    private int runtime;

    private boolean adult;

    private String overview;

    private int popularity;

    private float vote_average;

    private int budget;

    private int revenue;

    private List<Genre> genres;
}
