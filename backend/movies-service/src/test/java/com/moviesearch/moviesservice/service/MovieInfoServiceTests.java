package com.moviesearch.moviesservice.service;

import com.moviesearch.moviesservice.dto.*;
import com.moviesearch.moviesservice.service.implementation.MovieInfoServiceImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MovieInfoServiceTests {

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private MovieInfoServiceImpl movieInfoService;

    private static CreditsFromApi creditsFromApi;
    private static ReviewsCollectionFromApi reviewsCollectionFromApi;
    private static MovieVideosCollectionFromApi movieVideosCollectionFromApi;
    private static ImagesCollectionFromApi imagesCollectionFromApi;
    private static GenresList genresList;

    @BeforeClass
    public static void prepareData() {
        ActorFromApi actorFromApi1 = ActorFromApi.builder().name("Actor 1").build();
        ActorFromApi actorFromApi2 = ActorFromApi.builder().name("Actor 2").build();
        creditsFromApi = CreditsFromApi.builder()
                .cast(Arrays.asList(actorFromApi1, actorFromApi2))
                .crew(new ArrayList<>())
                .build();

        ReviewAuthorFromApi reviewAuthorFromApi = ReviewAuthorFromApi.builder().username("author").build();
        ReviewFromApi reviewFromApi1 = ReviewFromApi.builder()
                .author_details(reviewAuthorFromApi)
                .created_at(LocalDateTime.now().toString() + "Z")
                .updated_at(LocalDateTime.now().toString() + "Z")
                .content("Review 1")
                .build();
        ReviewFromApi reviewFromApi2 = ReviewFromApi.builder()
                .author_details(reviewAuthorFromApi)
                .created_at(LocalDateTime.now().toString() + "Z")
                .updated_at(LocalDateTime.now().toString() + "Z")
                .content("Review 2")
                .build();
        reviewsCollectionFromApi = ReviewsCollectionFromApi.builder()
                .results(Arrays.asList(reviewFromApi1, reviewFromApi2))
                .build();

        MovieVideoFromApi movieVideoFromApi1 = MovieVideoFromApi.builder().key("Video1").build();
        MovieVideoFromApi movieVideoFromApi2 = MovieVideoFromApi.builder().key("Video2").build();
        movieVideosCollectionFromApi = MovieVideosCollectionFromApi.builder()
                .results(Arrays.asList(movieVideoFromApi1, movieVideoFromApi2))
                .build();

        ImageFromApi imageFromApi1 = ImageFromApi.builder().file_path("Image1").build();
        ImageFromApi imageFromApi2 = ImageFromApi.builder().file_path("Image2").build();
        imagesCollectionFromApi = ImagesCollectionFromApi.builder()
                .posters(Arrays.asList(imageFromApi1, imageFromApi2))
                .backdrops(new ArrayList<>())
                .build();

        Genre genre1 = Genre.builder().id(1).name("Genre1").build();
        Genre genre2 = Genre.builder().id(2).name("Genre2").build();
        genresList = GenresList.builder()
                .genres(Arrays.asList(genre1, genre2))
                .build();
    }

    @Test
    public void getCreditsByMovieId_validQuery_shouldReturnCredits() {
        when(restTemplate.getForObject(anyString(), eq(CreditsFromApi.class), anyMap())).thenReturn(creditsFromApi);

        Credits credits = movieInfoService.getCreditsByMovieId(1L);

        assertEquals(creditsFromApi.getCast().size(), credits.getActors().size());
        assertEquals(creditsFromApi.getCast().get(0).getName(), credits.getActors().get(0).getName());
    }

    @Test
    public void getReviewByMovieId_validQuery_shouldReturnReviewsCollection() {
        when(restTemplate.getForObject(anyString(), eq(ReviewsCollectionFromApi.class), anyMap())).thenReturn(reviewsCollectionFromApi);

        ReviewsCollection reviewsCollection = movieInfoService.getReviewsByMovieId(1L, 1);

        assertEquals(reviewsCollectionFromApi.getResults().size(), reviewsCollection.getResults().size());
        assertEquals(reviewsCollectionFromApi.getResults().get(0).getContent(), reviewsCollection.getResults().get(0).getContent());
    }

    @Test
    public void getVideosByMovieId_validQuery_shouldReturnMovieVideos() {
        when(restTemplate.getForObject(anyString(), eq(MovieVideosCollectionFromApi.class), anyMap())).thenReturn(movieVideosCollectionFromApi);

        List<MovieVideo> movieVideos = movieInfoService.getVideosByMovieId(1L);

        assertEquals(movieVideosCollectionFromApi.getResults().size(), movieVideos.size());
        assertTrue(movieVideos.get(0).getPath().endsWith(movieVideosCollectionFromApi.getResults().get(0).getKey()));
    }

    @Test
    public void getImagesByMovieId_validQuery_shouldReturnImagesCollection() {
        when(restTemplate.getForObject(anyString(), eq(ImagesCollectionFromApi.class), anyMap())).thenReturn(imagesCollectionFromApi);

        ImagesCollection imagesCollection = movieInfoService.getImagesByMovieId(1L);

        assertEquals(imagesCollectionFromApi.getPosters().size(), imagesCollection.getPosters().size());
        assertTrue(imagesCollection.getPosters().get(0).getFilePath().endsWith(imagesCollectionFromApi.getPosters().get(0).getFile_path()));
    }

    @Test
    public void getAllGenres_validQuery_shouldReturnListOfGenres() {
        when(restTemplate.getForObject(anyString(), eq(GenresList.class))).thenReturn(genresList);

        List<Genre> actualGenres = movieInfoService.getAllGenres();

        assertEquals(genresList.getGenres().size(), actualGenres.size());
        assertEquals(genresList.getGenres().get(0).getName(), actualGenres.get(0).getName());
    }

    @Test
    public void getAllGenres_invalidQuery_shouldReturnNull() {
        when(restTemplate.getForObject(anyString(), eq(GenresList.class))).thenThrow(RestClientException.class);

        List<Genre> actualGenres = movieInfoService.getAllGenres();

        assertNull(actualGenres);
    }

    @Test
    public void getGenreById_validQueryAndExistingGenre_shouldReturnGenre() {
        Genre expectedGenre = genresList.getGenres().get(0);
        when(restTemplate.getForObject(anyString(), eq(GenresList.class))).thenReturn(genresList);

        Genre actualGenre = movieInfoService.getGenreById(expectedGenre.getId());

        assertNotNull(actualGenre);
        assertEquals(expectedGenre.getName(), actualGenre.getName());
        assertEquals(expectedGenre.getId(), actualGenre.getId());
    }

    @Test
    public void getGenreById_validQueryAndNonExistingGenre_shouldReturnNull() {
        when(restTemplate.getForObject(anyString(), eq(GenresList.class))).thenReturn(genresList);

        Genre actualGenre = movieInfoService.getGenreById(10);

        assertNull(actualGenre);
    }

    @Test
    public void getGenreById_invalidQuery_shouldReturnNull() {
        when(restTemplate.getForObject(anyString(), eq(GenresList.class))).thenThrow(RestClientException.class);

        Genre actualGenre = movieInfoService.getGenreById(10);

        assertNull(actualGenre);
    }
}
