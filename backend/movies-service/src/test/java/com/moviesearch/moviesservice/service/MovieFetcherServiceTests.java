package com.moviesearch.moviesservice.service;

import com.moviesearch.moviesservice.dto.*;
import com.moviesearch.moviesservice.service.implementation.MovieFetcherServiceImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MovieFetcherServiceTests {

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private MovieInfoService movieInfoService;

    @InjectMocks
    private MovieFetcherServiceImpl movieFetcherService;

    private static MovieDetailsFromApi movieDetailsFromApi;
    private static Credits credits;
    private static List<MovieVideo> movieVideosList;
    private static ImagesCollection imagesCollection;
    private static ReviewsCollection reviewsCollection;
    private static MoviesCollectionFromApi moviesCollectionFromApi;
    private static GenresList genresList;

    @BeforeClass
    public static void prepareData() {
        movieDetailsFromApi = MovieDetailsFromApi.builder().title("Movie from api").build();
        
        credits = Credits.builder()
                .actors(Arrays.asList(Actor.builder().name("Actor 1").build(), Actor.builder().name("Actor 2").build()))
                .build();

        movieVideosList = Arrays.asList(MovieVideo.builder().path("Video1").build(), MovieVideo.builder().path("Video2").build());

        imagesCollection = ImagesCollection.builder()
                .posters(Arrays.asList(Image.builder().filePath("Image1").build(), Image.builder().filePath("Image2").build()))
                .build();

        reviewsCollection = ReviewsCollection.builder()
                .results(Arrays.asList(Review.builder().content("Review 1").build(), Review.builder().content("Review 2").build()))
                .build();

        MovieBasicFromApi movieBasicFromApi1 = MovieBasicFromApi.builder()
                .title("Movie basic 1")
                .genre_ids(Arrays.asList(1, 2, 5))
                .release_date(LocalDate.now().toString())
                .build();
        MovieBasicFromApi movieBasicFromApi2 = MovieBasicFromApi.builder()
                .title("Movie basic 2")
                .genre_ids(Arrays.asList(1, 2, 3))
                .release_date(LocalDate.now().toString())
                .build();
        moviesCollectionFromApi = MoviesCollectionFromApi.builder()
                .results(Arrays.asList(movieBasicFromApi1, movieBasicFromApi2))
                .build();

        Genre genre1 = Genre.builder().name("Genre1").build();
        Genre genre2 = Genre.builder().name("Genre2").build();
        genresList = GenresList.builder().genres(Arrays.asList(genre1, genre2)).build();
    }

    @Test
    public void getMovieFullDetailsById_validMovieQuery_shouldReturnMovieFullDetails() {
        long movieId = 1L;
        when(restTemplate.getForObject(anyString(), eq(MovieDetailsFromApi.class), anyMap())).thenReturn(movieDetailsFromApi);
        when(movieInfoService.getCreditsByMovieId(movieId)).thenReturn(credits);
        when(movieInfoService.getVideosByMovieId(movieId)).thenReturn(movieVideosList);
        when(movieInfoService.getImagesByMovieId(movieId)).thenReturn(imagesCollection);
        when(movieInfoService.getReviewsByMovieId(movieId, 1)).thenReturn(reviewsCollection);

        MovieFullDetails movieFullDetails = movieFetcherService.getMovieFullDetailsById(movieId);

        assertEquals(movieDetailsFromApi.getTitle(), movieFullDetails.getMovie().getTitle());
        assertEquals(credits.getActors().size(), movieFullDetails.getCredits().getActors().size());
        assertEquals(credits.getActors().get(0).getName(), movieFullDetails.getCredits().getActors().get(0).getName());
        assertEquals(movieVideosList.size(), movieFullDetails.getVideos().size());
        assertEquals(movieVideosList.get(0).getPath(), movieFullDetails.getVideos().get(0).getPath());
        assertEquals(imagesCollection.getPosters().size(), movieFullDetails.getImages().getPosters().size());
        assertEquals(imagesCollection.getPosters().get(0).getFilePath(), movieFullDetails.getImages().getPosters().get(0).getFilePath());
        assertEquals(reviewsCollection.getResults().get(0).getContent(), movieFullDetails.getReview().getContent());
    }

    @Test
    public void getMovieFullDetailsById_invalidMovieQuery_shouldReturnNull() {
        when(restTemplate.getForObject(anyString(), eq(MovieDetailsFromApi.class), anyMap())).thenThrow(RestClientException.class);

        MovieFullDetails movieFullDetails = movieFetcherService.getMovieFullDetailsById(1L);

        assertNull(movieFullDetails);
    }

    @Test
    public void getMovieFullDetailsById_nullMovieId_shouldThrowException() {
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> movieFetcherService.getMovieFullDetailsById(null));
        assertNotNull(exception.getMessage());
    }

    @Test
    public void getPopularMovies_validMovieQuery_shouldReturnMoviesCollection() {
        when(restTemplate.getForObject(anyString(), eq(MoviesCollectionFromApi.class), anyMap())).thenReturn(moviesCollectionFromApi);
        when(movieInfoService.getGenreById(anyInt())).thenReturn(genresList.getGenres().get(0));

        MoviesCollection moviesCollection = movieFetcherService.getPopularMovies(1);

        assertEquals(moviesCollectionFromApi.getResults().size(), moviesCollection.getMovies().size());
        assertEquals(moviesCollectionFromApi.getResults().get(0).getTitle(), moviesCollection.getMovies().get(0).getTitle());
    }

    @Test
    public void getPopularMovies_invalidMovieQuery_shouldReturnNull() {
        when(restTemplate.getForObject(anyString(), eq(MoviesCollectionFromApi.class), anyMap())).thenThrow(RestClientException.class);

        MoviesCollection moviesCollection = movieFetcherService.getPopularMovies(1);

        assertNull(moviesCollection);
    }

    @Test
    public void getPopularMovies_nullPage_shouldThrowException() {
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> movieFetcherService.getPopularMovies(null));
        assertNotNull(exception.getMessage());
    }

    @Test
    public void getHomePageMovies_validMovieQuery_shouldReturnMoviesCollection() {
        when(restTemplate.getForObject(anyString(), eq(MoviesCollectionFromApi.class), anyMap())).thenReturn(moviesCollectionFromApi);
        when(movieInfoService.getGenreById(anyInt())).thenReturn(genresList.getGenres().get(0));

        MoviesCollection moviesCollection = movieFetcherService.getHomePageMovies();

        assertEquals(moviesCollectionFromApi.getResults().size(), moviesCollection.getMovies().size());
        assertEquals(moviesCollectionFromApi.getResults().get(0).getTitle(), moviesCollection.getMovies().get(0).getTitle());
    }

    @Test
    public void getHomePageMovies_invalidMovieQuery_shouldReturnNull() {
        when(restTemplate.getForObject(anyString(), eq(MoviesCollectionFromApi.class), anyMap())).thenThrow(RestClientException.class);

        MoviesCollection moviesCollection = movieFetcherService.getHomePageMovies();

        assertNull(moviesCollection);
    }

    @Test
    public void getNowPlayingMovies_validMovieQuery_shouldReturnMoviesCollection() {
        when(restTemplate.getForObject(anyString(), eq(MoviesCollectionFromApi.class), anyMap())).thenReturn(moviesCollectionFromApi);
        when(movieInfoService.getGenreById(anyInt())).thenReturn(genresList.getGenres().get(0));

        MoviesCollection moviesCollection = movieFetcherService.getNowPlayingMovies(1);

        assertEquals(moviesCollectionFromApi.getResults().size(), moviesCollection.getMovies().size());
        assertEquals(moviesCollectionFromApi.getResults().get(0).getTitle(), moviesCollection.getMovies().get(0).getTitle());
    }

    @Test
    public void getNowPlayingMovies_invalidMovieQuery_shouldReturnNull() {
        when(restTemplate.getForObject(anyString(), eq(MoviesCollectionFromApi.class), anyMap())).thenThrow(RestClientException.class);

        MoviesCollection moviesCollection = movieFetcherService.getNowPlayingMovies(1);

        assertNull(moviesCollection);
    }

    @Test
    public void getNowPlayingMovies_nullPage_shouldThrowException() {
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> movieFetcherService.getNowPlayingMovies(null));
        assertNotNull(exception.getMessage());
    }

    @Test
    public void getUpcomingMovies_validMovieQuery_shouldReturnMoviesCollection() {
        when(restTemplate.getForObject(anyString(), eq(MoviesCollectionFromApi.class), anyMap())).thenReturn(moviesCollectionFromApi);
        when(movieInfoService.getGenreById(anyInt())).thenReturn(genresList.getGenres().get(0));

        MoviesCollection moviesCollection = movieFetcherService.getUpcomingMovies(1);

        assertEquals(moviesCollectionFromApi.getResults().size(), moviesCollection.getMovies().size());
        assertEquals(moviesCollectionFromApi.getResults().get(0).getTitle(), moviesCollection.getMovies().get(0).getTitle());
    }

    @Test
    public void getUpcomingMovies_invalidMovieQuery_shouldReturnNull() {
        when(restTemplate.getForObject(anyString(), eq(MoviesCollectionFromApi.class), anyMap())).thenThrow(RestClientException.class);

        MoviesCollection moviesCollection = movieFetcherService.getUpcomingMovies(1);

        assertNull(moviesCollection);
    }

    @Test
    public void getUpcomingMovies_nullPage_shouldThrowException() {
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> movieFetcherService.getUpcomingMovies(null));
        assertNotNull(exception.getMessage());
    }

    @Test
    public void getTopRatedMovies_validMovieQuery_shouldReturnMoviesCollection() {
        when(restTemplate.getForObject(anyString(), eq(MoviesCollectionFromApi.class), anyMap())).thenReturn(moviesCollectionFromApi);
        when(movieInfoService.getGenreById(anyInt())).thenReturn(genresList.getGenres().get(0));

        MoviesCollection moviesCollection = movieFetcherService.getTopRatedMovies(1);

        assertEquals(moviesCollectionFromApi.getResults().size(), moviesCollection.getMovies().size());
        assertEquals(moviesCollectionFromApi.getResults().get(0).getTitle(), moviesCollection.getMovies().get(0).getTitle());
    }

    @Test
    public void getTopRatedMovies_invalidMovieQuery_shouldReturnNull() {
        when(restTemplate.getForObject(anyString(), eq(MoviesCollectionFromApi.class), anyMap())).thenThrow(RestClientException.class);

        MoviesCollection moviesCollection = movieFetcherService.getTopRatedMovies(1);

        assertNull(moviesCollection);
    }

    @Test
    public void getTopRatedMovies_nullPage_shouldThrowException() {
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> movieFetcherService.getTopRatedMovies(null));
        assertNotNull(exception.getMessage());
    }
}
