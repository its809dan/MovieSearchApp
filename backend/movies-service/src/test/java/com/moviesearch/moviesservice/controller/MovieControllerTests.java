package com.moviesearch.moviesservice.controller;

import com.moviesearch.moviesservice.dto.MovieBasic;
import com.moviesearch.moviesservice.dto.MovieDetails;
import com.moviesearch.moviesservice.dto.MovieFullDetails;
import com.moviesearch.moviesservice.dto.MoviesCollection;
import com.moviesearch.moviesservice.service.MovieFetcherService;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MovieControllerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private MovieFetcherService movieFetcherService;

    private static MovieFullDetails movieFullDetails;
    private static MoviesCollection moviesCollection;

    @BeforeClass
    public static void prepareData() {
        movieFullDetails = MovieFullDetails.builder()
                .movie(MovieDetails.builder().title("Movie").build())
                .build();

        MovieBasic movieBasic1 = MovieBasic.builder().title("Movie 1").build();
        MovieBasic movieBasic2 = MovieBasic.builder().title("Movie 2").build();
        moviesCollection = MoviesCollection.builder()
                .movies(Arrays.asList(movieBasic1, movieBasic2))
                .build();
    }

    @Test
    public void getMovieFullDetails_validMovieId_shouldReturnMovieFullDetails() throws Exception {
        Long movieId = 1L;
        given(movieFetcherService.getMovieFullDetailsById(movieId)).willReturn(movieFullDetails);

        mvc.perform(get(String.format("/movies/%d", movieId))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.movie.title", is(movieFullDetails.getMovie().getTitle())));
    }

    @Test
    public void getMovieFullDetails_nonExistingMovieId_shouldReturnEmptyBody() throws Exception {
        Long movieId = 1L;
        given(movieFetcherService.getMovieFullDetailsById(movieId)).willReturn(null);

        mvc.perform(get(String.format("/movies/%d", movieId))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    public void getMovieFullDetails_invalidMovieId_shouldReturnErrorResponse() throws Exception {
        String movieId = "movieId";
        given(movieFetcherService.getMovieFullDetailsById(anyLong())).willReturn(null);

        mvc.perform(get(String.format("/movies/%s", movieId))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", notNullValue()))
                .andExpect(jsonPath("$.timestamp", notNullValue()));
    }

    @Test
    public void getPopularMovies_validPage_shouldReturnPopularMoviesCollection() throws Exception {
        int page = 1;
        given(movieFetcherService.getPopularMovies(page)).willReturn(moviesCollection);

        mvc.perform(get("/movies/popular")
                .param("page", Integer.toString(page))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.movies", hasSize(moviesCollection.getMovies().size())))
                .andExpect(jsonPath("$.movies[0].title", is(moviesCollection.getMovies().get(0).getTitle())));
    }

    @Test
    public void getPopularMovies_invalidPage_shouldReturnErrorResponse() throws Exception {
        String page = "page";
        given(movieFetcherService.getPopularMovies(anyInt())).willReturn(moviesCollection);

        mvc.perform(get("/movies/popular")
                .param("page", page)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", notNullValue()))
                .andExpect(jsonPath("$.timestamp", notNullValue()));
    }

    @Test
    public void getHomePageMovies_shouldReturnHomePageMoviesCollection() throws Exception {
        given(movieFetcherService.getHomePageMovies()).willReturn(moviesCollection);

        mvc.perform(get("/movies/home")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.movies", hasSize(moviesCollection.getMovies().size())))
                .andExpect(jsonPath("$.movies[0].title", is(moviesCollection.getMovies().get(0).getTitle())));
    }

    @Test
    public void getNowPlayingMovies_validPage_shouldReturnNowPlayingMoviesCollection() throws Exception {
        int page = 1;
        given(movieFetcherService.getNowPlayingMovies(page)).willReturn(moviesCollection);

        mvc.perform(get("/movies/now")
                .param("page", Integer.toString(page))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.movies", hasSize(moviesCollection.getMovies().size())))
                .andExpect(jsonPath("$.movies[0].title", is(moviesCollection.getMovies().get(0).getTitle())));
    }

    @Test
    public void getNowPlayingMovies_invalidPage_shouldReturnErrorResponse() throws Exception {
        String page = "page";
        given(movieFetcherService.getPopularMovies(anyInt())).willReturn(moviesCollection);

        mvc.perform(get("/movies/now")
                .param("page", page)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", notNullValue()))
                .andExpect(jsonPath("$.timestamp", notNullValue()));
    }

    @Test
    public void getUpcomingMovies_validPage_shouldReturnUpcomingMoviesCollection() throws Exception {
        int page = 1;
        given(movieFetcherService.getUpcomingMovies(page)).willReturn(moviesCollection);

        mvc.perform(get("/movies/upcoming")
                .param("page", Integer.toString(page))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.movies", hasSize(moviesCollection.getMovies().size())))
                .andExpect(jsonPath("$.movies[0].title", is(moviesCollection.getMovies().get(0).getTitle())));
    }

    @Test
    public void getUpcomingMovies_invalidPage_shouldReturnErrorResponse() throws Exception {
        String page = "page";
        given(movieFetcherService.getPopularMovies(anyInt())).willReturn(moviesCollection);

        mvc.perform(get("/movies/upcoming")
                .param("page", page)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", notNullValue()))
                .andExpect(jsonPath("$.timestamp", notNullValue()));
    }

    @Test
    public void getTopRatedMovies_validPage_shouldReturnTopRatedMoviesCollection() throws Exception {
        int page = 1;
        given(movieFetcherService.getTopRatedMovies(page)).willReturn(moviesCollection);

        mvc.perform(get("/movies/top")
                .param("page", Integer.toString(page))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.movies", hasSize(moviesCollection.getMovies().size())))
                .andExpect(jsonPath("$.movies[0].title", is(moviesCollection.getMovies().get(0).getTitle())));
    }

    @Test
    public void getTopRatedMovies_invalidPage_shouldReturnErrorResponse() throws Exception {
        String page = "page";
        given(movieFetcherService.getPopularMovies(anyInt())).willReturn(moviesCollection);

        mvc.perform(get("/movies/top")
                .param("page", page)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", notNullValue()))
                .andExpect(jsonPath("$.timestamp", notNullValue()));
    }
}
